package jsonrRsources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import arka.domain.Carton;
import arka.domain.Client;

public class JsonParserCarton {
		public static List<Carton> getCartonList(String URl) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URl);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readCarton(response);
		Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type type = new TypeToken<List<Carton>>(){}.getType();
		return gson.fromJson(data,type);
	}
		public static Carton getCarton(String URl) throws ClientProtocolException, IOException {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(URl);
			httpGet.addHeader("accept", "application/json");
			HttpResponse response= httpClient.execute(httpGet);
			String data = readCarton(response);
			Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			Type type = new TypeToken<Carton>(){}.getType();
			return gson.fromJson(data,type);
		}
		public static List<Client> getClientList(String URl) throws ClientProtocolException, IOException {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(URl);
			httpGet.addHeader("accept", "application/json");
			HttpResponse response= httpClient.execute(httpGet);
			String data = readCarton(response);
			Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			Type type = new TypeToken<List<Client>>(){}.getType();
			return gson.fromJson(data,type);
		}
		public  static void add_Carton(int idclient,int id,String entree,String destruction,long i) throws IOException, JAXBException
		{
			 
		/*String uri =
			    "http://localhost:18080/Arka-web/api/carton/"+idclient+"/"+id+"/"+entree+"/"+destruction+"/"+i;
			URL url = new URL(uri);
			HttpURLConnection connection =
			    (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Accept", "application/json");

			JAXBContext jc = JAXBContext.newInstance(Carton.class);
			InputStream xml = connection.getInputStream();
			Carton customer =
			    (Carton) jc.createUnmarshaller().unmarshal(xml);

			connection.disconnect();*/
			try {

				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost postRequest = new HttpPost(
					"http://localhost:18080/Arka-web/api/carton/"+idclient+"/"+id+"/"+entree+"/"+destruction+"/"+i);

				//StringEntity input = new StringEntity("{\"qty\":100,\"name\":\"iPad 4\"}");
				//input.setContentType("application/json");
				//postRequest.setEntity(input);

				HttpResponse response = httpClient.execute(postRequest);

				if (response.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
				}

				BufferedReader br = new BufferedReader(
		                        new InputStreamReader((response.getEntity().getContent())));

				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
				}

				httpClient.getConnectionManager().shutdown();

			  } catch (MalformedURLException e) {

				e.printStackTrace();
			
			  } catch (IOException e) {

				e.printStackTrace();

			  }

			}
		public  static void add_Carton_to_location(int idcarton,int idlocation) throws IOException, JAXBException
		{
			 
		
			try {

				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost postRequest = new HttpPost(
					"http://localhost:18080/Arka-web/api/carton/remplir/"+idcarton+"/"+idlocation);

				

				HttpResponse response = httpClient.execute(postRequest);

				if (response.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
				}

				BufferedReader br = new BufferedReader(
		                        new InputStreamReader((response.getEntity().getContent())));

				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
				}

				httpClient.getConnectionManager().shutdown();

			  } catch (MalformedURLException e) {

				e.printStackTrace();
			
			  } catch (IOException e) {

				e.printStackTrace();

			  }

			}
		
	public static String readCarton(HttpResponse response) throws IOException{
		BufferedReader bufferedReader =null;
		try{
			bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer buffer= new StringBuffer();
			char[] datalength= new char[1024];
			int read;
			while((read=bufferedReader.read(datalength))!= -1){
				buffer.append(datalength,0,read);
				
			}
			return buffer.toString();
		}catch(Exception e){
			throw e;
			
		}finally {
			if(bufferedReader != null){
				
				bufferedReader.close();
			}
		}
		
	}
}
