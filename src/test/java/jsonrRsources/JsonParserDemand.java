package jsonrRsources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import arka.domain.Demand;
import arka.domain.DemandState;
import arka.domain.DemandType;

public class JsonParserDemand {
	public static Demand getDemand(String URl) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URl);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readDemand(response);
		Gson gson=new Gson();
		return gson.fromJson(data,Demand.class);
	}
	
	public static List<Demand> getDemandList(String URl) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URl);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readDemand(response);
		Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type type = new TypeToken<List<Demand>>(){}.getType();
		return gson.fromJson(data,type);
	}
	
	public static List<Demand> getDemandListByCLient(String URl,int IdClient) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URl+"/"+IdClient);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readDemand(response);
		Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type type = new TypeToken<List<Demand>>(){}.getType();
		return gson.fromJson(data,type);
	}
	
	public static List<Demand> getDemandListByCLientName(String URl,String clientName) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URl+"/"+clientName);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readDemand(response);
		Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type type = new TypeToken<List<Demand>>(){}.getType();
		return gson.fromJson(data,type);
	}
	
	public static List<Demand> getDemandListByAgent(String URl,String IdClient) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URl+"/"+IdClient);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readDemand(response);
		Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type type = new TypeToken<List<Demand>>(){}.getType();
		return gson.fromJson(data,type);
	}
	
	public static List<Demand> getByType(String URl,DemandType demandType) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URl+"/"+demandType);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readDemand(response);
		Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type type = new TypeToken<List<Demand>>(){}.getType();
		return gson.fromJson(data,type);
	}
	
	public static List<Demand> getByDate(String URl,LocalDate date) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URl+"/"+date);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readDemand(response);
		Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type type = new TypeToken<List<Demand>>(){}.getType();
		return gson.fromJson(data,type);
	}
	
	public static List<Demand> getByState(String URl,DemandState demandState) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URl+"/"+demandState);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readDemand(response);
		Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type type = new TypeToken<List<Demand>>(){}.getType();
		return gson.fromJson(data,type);
	}
	
	public static void changeDemandState(String URl,int idDemand) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URl+"/traitee/"+idDemand);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readDemand(response);
		Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type type = new TypeToken<List<Demand>>(){}.getType();
		
	}
	
	public static void affecter(String URl,int idDemand,String matricule ) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost httpGet = new HttpPost(URl+"/"+matricule+"/"+idDemand);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readDemand(response);
		Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type type = new TypeToken<List<Demand>>(){}.getType();
		
	}
	
	public static String readDemand(HttpResponse response) throws IOException{
		BufferedReader bufferedReader =null;
		try{
			bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer buffer= new StringBuffer();
			char[] datalength= new char[1024];
			int read;
			while((read=bufferedReader.read(datalength))!= -1){
				buffer.append(datalength,0,read);
				
			}
			return buffer.toString();
		}catch(Exception e){
			throw e;
			
		}finally {
			if(bufferedReader != null){
				
				bufferedReader.close();
			}
		}
		
	}
}
		

