package jsonrRsources;
import javax.xml.bind.JAXBException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import arka.domain.Agent;
import arka.domain.Admin;
public class JsonParserAuth {

	public  static  String auth (String URl) throws IOException, JAXBException
    {
	
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(URl);
			httpGet.addHeader("accept", "application/json");
			HttpResponse response= httpClient.execute(httpGet);
			String data = readAgent(response);
			//Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			//Type type = new TypeToken<>(){}.getType();
			return data;
    }
	
	public static String readAgent(HttpResponse response) throws IOException{
		BufferedReader bufferedReader =null;
		try{
			bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer buffer= new StringBuffer();
			char[] datalength= new char[1024];
			int read;
			while((read=bufferedReader.read(datalength))!= -1){
				buffer.append(datalength,0,read);
				
			}
			return buffer.toString();
		}catch(Exception e){
			throw e;
			
		}finally {
			if(bufferedReader != null){
				
				bufferedReader.close();
			}
		}
		
	}


	public  static  int id (String URl) throws IOException, JAXBException
    {
	
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(URl);
			httpGet.addHeader("accept", "application/json");
			HttpResponse response= httpClient.execute(httpGet);
			int data =Integer.parseInt( readid(response));
			//Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			//Type type = new TypeToken<>(){}.getType();
			return data;
    }
	public static String readid(HttpResponse response) throws IOException{
		BufferedReader bufferedReader =null;
		try{
			bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer buffer= new StringBuffer();
			char[] datalength= new char[1024];
			int read;
			while((read=bufferedReader.read(datalength))!= -1){
				buffer.append(datalength,0,read);
				
			}
			return buffer.toString();
		}catch(Exception e){
			throw e;
			
		}finally {
			if(bufferedReader != null){
				
				bufferedReader.close();
			}
		}
		
	}


}
