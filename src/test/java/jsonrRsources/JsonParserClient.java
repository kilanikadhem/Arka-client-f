package jsonrRsources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import arka.domain.Client;

public class JsonParserClient {

	public static List<Client> getClientList(String URl) throws ClientProtocolException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URl);
		httpGet.addHeader("accept", "application/json");
		HttpResponse response= httpClient.execute(httpGet);
		String data = readAdmin(response);
		Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type type = new TypeToken<List<Client>>(){}.getType();
		return gson.fromJson(data,type);
	}

	public static String readAdmin(HttpResponse response) throws IOException{
		BufferedReader bufferedReader =null;
		try{
			bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer buffer= new StringBuffer();
			char[] datalength= new char[1024];
			int read;
			while((read=bufferedReader.read(datalength))!= -1){
				buffer.append(datalength,0,read);
				
			}
			return buffer.toString();
		}catch(Exception e){
			throw e;
			
		}finally {
			if(bufferedReader != null){
				
				bufferedReader.close();
			}
		}
	}
	
	
	
	
	
	public  static void addClient(String code,String nom,String password,String email,String nomresp,String numTel) throws IOException, JAXBException
	{
         
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(
                "http://localhost:18080/Arka-web/api/client/addCLient/"+code+"/"+nom+"/"+password+"/"+email+"/"+nomresp+"/"+numTel);
            HttpResponse response = httpClient.execute(postRequest);
            BufferedReader br = new BufferedReader(
                new InputStreamReader((response.getEntity().getContent())));
           
            String output;
         while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
            httpClient.getConnectionManager().shutdown();
          } catch (MalformedURLException e) {
            e.printStackTrace();
        
          } catch (IOException e) {
            e.printStackTrace();
          }
    }
	
	
	
	  public  static void DeleteClient(int idclient) throws IOException, JAXBException
      {
           
          try {
              DefaultHttpClient httpClient = new DefaultHttpClient();
              HttpDelete deleteRequest = new HttpDelete(
                  "http://localhost:18080/Arka-web/api/client/"+idclient);
             
              HttpResponse response = httpClient.execute(deleteRequest);
              BufferedReader br = new BufferedReader(
                              new InputStreamReader((response.getEntity().getContent())));
              String output;
              System.out.println("Output from Server .... \n");
              while ((output = br.readLine()) != null) {
                  System.out.println(output);
              }
              httpClient.getConnectionManager().shutdown();
            } catch (MalformedURLException e) {
              e.printStackTrace();
          
            } catch (IOException e) {
              e.printStackTrace();
            }
          

  }


}
