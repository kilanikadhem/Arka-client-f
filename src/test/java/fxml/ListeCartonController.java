/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import javafx.geometry.Pos;
import arka.domain.*;
import java.io.IOException;
import java.net.URL;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.util.Callback;
import javax.xml.crypto.Data;

import org.apache.http.client.ClientProtocolException;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jsonrRsources.JsonParserCarton;
import jsonrRsources.JsonParserDemand;
import jsonrRsources.JsonParserEmplacement;
import javafx.scene.layout.HBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author hafedh
 */
public class ListeCartonController implements Initializable {

    /**
     * Initializes the controller class.
     */
	@FXML
    private JFXTextField id_recherchercarton;
	@FXML
    private JFXTextField id_nomclient;
	@FXML
    private JFXButton id_recherchecarton;
	@FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton CartonBtn;
    @FXML
    private JFXButton DemandesBtn;
    @FXML
    private JFXButton SitesBtn;
    
    @FXML
    private JFXButton PersonnelBtn;
	@FXML
    private ImageView imageHead;
	@FXML
    private Text id_imagecaptb;
	@FXML
    private ImageView id_imagecarton;
	@FXML
    private ImageView cartonsImg;
	
	@FXML
    private ImageView id_imagerecherche;
	@FXML
    private ImageView HomeImg;
	@FXML
    private ImageView demandesImg;
	@FXML
    private ImageView sitesImg;
	@FXML
    private ImageView personnelImg;
	@FXML
    private ImageView c;
	@FXML
    private ImageView statImg;
	@FXML
    private ImageView id_pluscarton;
	@FXML
    private ImageView id_imgdate;
	
	
	@FXML
    private JFXButton id_ajoutercarton;
	@FXML
    private JFXListView<Carton> id_listecarton;
    
    private ObservableList<Carton> data=FXCollections.observableArrayList();
    
    private ObservableList<Location> data_location=FXCollections.observableArrayList();
    
    @FXML
    private void ajouterCarton(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Carton.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }
    @FXML
    private void homeAction(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void cartonsBtn(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListeCarton.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void demandesAction(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdminDemand.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void sitesActions(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListSite.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }
    @FXML
    private void personnelAction(ActionEvent event) {
    }
    @FXML
    private int RechercherCarton(ActionEvent event) throws IOException {
    	ObservableList<Carton> data1=FXCollections.observableArrayList();
        
        ObservableList<Location> data_location1=FXCollections.observableArrayList();
        id_listecarton.setItems(data1);
    	if((id_recherchercarton.getText().equals("")==false &&id_nomclient.getText().equals("")==true))
	    {
    		List<Location> locs1 = new ArrayList<Location>();
    		try {
    			locs1 = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location/empty");
    		} catch (ClientProtocolException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	
            Collections.reverse(locs1);
           for (Location location : locs1 ) {
            	    
            	   
        	   data_location1.add(location);		
    			  }
    		
    		/*for (Client client : clients) {
    			System.out.print(client.getNom());
                  data_nom_client.add(client.getNom());
    		}*/
    		//List<Carton> cartons_lst1 = new ArrayList<Carton>();
           Carton crt1 = null ;
    		try {
    			crt1 = JsonParserCarton.getCarton("http://localhost:18080/Arka-web/api/carton/rech1/"+id_recherchercarton.getText());
    		} catch (ClientProtocolException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
     	
             //Collections.reverse(crt1);
            //for (Carton carton : cartons_lst1 ) {
             	   // System.out.print(carton.getIdCarton());
             	   /*if(crt1==null)
             	   {
             		  Alert alert = new Alert(Alert.AlertType.ERROR);
                      alert.setHeaderText("CAPTB");
                      alert.setTitle("Confirmation");
                   
                      alert.initModality(Modality.APPLICATION_MODAL);
                      alert.setContentText("Les info sont �rron�es");
                      		
                      Button exitButton = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
                      exitButton.setText("Oui");
                      Optional<ButtonType> resu = alert.showAndWait(); 
             		  	 
             	   
             	   }*/
             	  data1.add(crt1);	 
     			 // }
               
            id_listecarton.setCellFactory(new Callback<ListView<Carton>, ListCell<Carton>>() {
    			@Override
    			public ListCell<Carton> call(ListView<Carton> args0) {
    				Image img = new Image("assets/archive.png");
    				ImageView imageview = new ImageView(img);
    				Label label = new Label("");
    				JFXButton buttinfo = new JFXButton();
    				// butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill:
    				// white;");
    				buttinfo.setStyle("-fx-background-image: url('/assets/info-.png');-fx-font-size: 1em; ");
    				buttinfo.setMinHeight(30);
    				buttinfo.setMinWidth(30);
    				
    				
    				JFXComboBox <Location>comboboxemplacement=new JFXComboBox(data_location);
    				
    				JFXButton buttAffecter = new JFXButton();
    				buttAffecter.setText("affecter emplacement");
    				buttAffecter.setStyle("-fx-background-color: #7f7fff;");
    				buttAffecter.setMinHeight(30);
    				buttAffecter.setMinWidth(50);
    				//buttAffecter.setStyle("-fx-background-image: url('/assets/send.png');");
    				buttAffecter.setAlignment(Pos.CENTER_RIGHT);
    				HBox hbox = new HBox();
    				hbox.getChildren().addAll(imageview, label, buttinfo,comboboxemplacement,buttAffecter);

    				ListCell<Carton> cell;
    				int i = 0;
    				cell = new ListCell<Carton>() {

    					@Override
    					protected void updateItem(Carton r, boolean b) {

    						super.updateItem(r, b);

    						if (r != null) {
    							buttinfo.setOnAction((event) -> {
    								FXMLLoader loader = new FXMLLoader(getClass().getResource("InfoCarton.fxml"));

    								Parent root;
    								try {
    									root = (Parent) loader.load();
    									InfoCartonController ctrl = loader.getController();
    									ctrl.setId_code(r.getIdCartonClient());
    									ctrl.setId_dateentree(r.getArrivalDate());
    									ctrl.setId_datedestruction(r.getDestructionDate());
    									ctrl.setId_duree(r.getDuration());
    									if(r.getClient()!=null)
    									{
    										ctrl.getId_affectclient().setStyle("-fx-background-color: #09e40d;");
    										ctrl.getId_affectclient().setText("Affect�e");
    									}
    									else
    									{
    										ctrl.getId_affectclient().setStyle("-fx-background-color:#ff9999;");
    										ctrl.getId_affectclient().setText("Non affect�e");
    									}
    									if(r.getLocale()!=null)
    									{
    										System.out.println(r.getLocale().getIdLocation());
    										ctrl.getId_affectemplacement().setStyle("-fx-background-color: #09e40d;");
    										ctrl.getId_affectemplacement().setText("Affect�e");
    									}
    									else
    									{
    										//System.out.println(r.getLocale().getIdLocation());
    										ctrl.getId_affectemplacement().setStyle("-fx-background-color:#ff9999;");
    										ctrl.getId_affectemplacement().setText("Non affect�e");
    									}
    									Scene newScene = new Scene(root);
    									Stage newStage = new Stage();
    									newStage.setTitle("Informations");
    									newStage.setScene(newScene);
    									newStage.show();
    								} catch (Exception e) {
    									// TODO Auto-generated catch block
    									e.printStackTrace();
    								}

    							});
    							buttAffecter.setOnAction((event) -> {
    								int idcarton=id_listecarton.getSelectionModel().getSelectedItem().getIdCarton();
    								int idlocation=comboboxemplacement.getValue().getIdLocation();
    								try {
    									JsonParserCarton.add_Carton_to_location(idcarton, idlocation);
    								} catch (Exception e) {
    									// TODO Auto-generated catch block
    									e.printStackTrace();
    								}

    							});
    							
    							if (r.getLocale() == null) {

    								hbox.setStyle("-fx-background-color:#ff9999;");
    							} 
    							 else {
    								
    								buttAffecter.setDisable(true);
    								hbox.setStyle("-fx-background-color:#09e40d;");
    								//hbox.setStyle("-fx-background-color:#bef67a;");

    							}
    							label.setText("Code carton client:" + r.getIdCartonClient()+" "+"Affect�e � "+r.getClient().getNom());
    							
    							setGraphic(hbox);
    						}

    					}

    				};

    				return cell;
    			}

    		});
            id_listecarton.setItems(data1);
				return 0;
	    	
	    	}
             	   
	    
    	if((id_recherchercarton.getText().equals("")==true &&id_nomclient.getText().equals("")==false))
        {
    		List<Location> locs2 = new ArrayList<Location>();
    		try {
    			locs2 = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location/empty");
    		} catch (ClientProtocolException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	
            Collections.reverse(locs2);
           for (Location location : locs2 ) {
            	    
            	   
        	   data_location1.add(location);		
    			  }
    		
    		/*for (Client client : clients) {
    			System.out.print(client.getNom());
                  data_nom_client.add(client.getNom());
    		}*/
    		List<Carton> cartons_lst2 = new ArrayList<Carton>();
    		try {
    			cartons_lst2 = JsonParserCarton.getCartonList("http://localhost:18080/Arka-web/api/carton/rech2/"+id_nomclient.getText());
    		} catch (ClientProtocolException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
     	
             Collections.reverse(cartons_lst2);
            for (Carton carton : cartons_lst2 ) {
             	    System.out.print(carton.getIdCarton());
             	   
             		data1.add(carton);		
     			  }
               
            id_listecarton.setCellFactory(new Callback<ListView<Carton>, ListCell<Carton>>() {
    			@Override
    			public ListCell<Carton> call(ListView<Carton> args0) {
    				Image img = new Image("assets/archive.png");
    				ImageView imageview = new ImageView(img);
    				Label label = new Label("");
    				JFXButton buttinfo = new JFXButton();
    				// butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill:
    				// white;");
    				buttinfo.setStyle("-fx-background-image: url('/assets/info-.png');-fx-font-size: 1em; ");
    				buttinfo.setMinHeight(30);
    				buttinfo.setMinWidth(30);
    				
    				
    				JFXComboBox <Location>comboboxemplacement=new JFXComboBox(data_location);
    				
    				JFXButton buttAffecter = new JFXButton();
    				buttAffecter.setText("affecter emplacement");
    				buttAffecter.setStyle("-fx-background-color: #7f7fff;");
    				buttAffecter.setMinHeight(30);
    				buttAffecter.setMinWidth(50);
    				//buttAffecter.setStyle("-fx-background-image: url('/assets/send.png');");
    				buttAffecter.setAlignment(Pos.CENTER_RIGHT);
    				HBox hbox = new HBox();
    				hbox.getChildren().addAll(imageview, label, buttinfo,comboboxemplacement,buttAffecter);

    				ListCell<Carton> cell;
    				int i = 0;
    				cell = new ListCell<Carton>() {

    					@Override
    					protected void updateItem(Carton r, boolean b) {

    						super.updateItem(r, b);

    						if (r != null) {
    							buttinfo.setOnAction((event) -> {
    								FXMLLoader loader = new FXMLLoader(getClass().getResource("InfoCarton.fxml"));

    								Parent root;
    								try {
    									root = (Parent) loader.load();
    									InfoCartonController ctrl = loader.getController();
    									ctrl.setId_code(r.getIdCartonClient());
    									ctrl.setId_dateentree(r.getArrivalDate());
    									ctrl.setId_datedestruction(r.getDestructionDate());
    									ctrl.setId_duree(r.getDuration());
    									if(r.getClient()!=null)
    									{
    										ctrl.getId_affectclient().setStyle("-fx-background-color: #09e40d;");
    										ctrl.getId_affectclient().setText("Affect�e");
    									}
    									else
    									{
    										ctrl.getId_affectclient().setStyle("-fx-background-color:#ff9999;");
    										ctrl.getId_affectclient().setText("Non affect�e");
    									}
    									if(r.getLocale()!=null)
    									{
    										System.out.println(r.getLocale().getIdLocation());
    										ctrl.getId_affectemplacement().setStyle("-fx-background-color: #09e40d;");
    										ctrl.getId_affectemplacement().setText("Affect�e");
    									}
    									else
    									{
    										//System.out.println(r.getLocale().getIdLocation());
    										ctrl.getId_affectemplacement().setStyle("-fx-background-color:#ff9999;");
    										ctrl.getId_affectemplacement().setText("Non affect�e");
    									}
    									Scene newScene = new Scene(root);
    									Stage newStage = new Stage();
    									newStage.setTitle("Informations");
    									newStage.setScene(newScene);
    									newStage.show();
    								} catch (Exception e) {
    									// TODO Auto-generated catch block
    									e.printStackTrace();
    								}

    							});
    							buttAffecter.setOnAction((event) -> {
    								int idcarton=id_listecarton.getSelectionModel().getSelectedItem().getIdCarton();
    								int idlocation=comboboxemplacement.getValue().getIdLocation();
    								try {
    									JsonParserCarton.add_Carton_to_location(idcarton, idlocation);
    								} catch (Exception e) {
    									// TODO Auto-generated catch block
    									e.printStackTrace();
    								}

    							});
    							
    							if (r.getLocale() == null) {

    								hbox.setStyle("-fx-background-color:#ff9999;");
    							} 
    							 else {
    								
    								buttAffecter.setDisable(true);
    								hbox.setStyle("-fx-background-color:#09e40d;");
    								//hbox.setStyle("-fx-background-color:#bef67a;");

    							}
    							label.setText("Code carton client:" + r.getIdCartonClient()+" "+"Affect�e � "+r.getClient().getNom());
    							
    							setGraphic(hbox);
    						}

    					}

    				};

    				return cell;
    			}

    		});
            id_listecarton.setItems(data1);
				return 0;
	    	
	    	}
    	 Alert alert = new Alert(Alert.AlertType.ERROR);
         alert.setHeaderText("CAPTB");
         alert.setTitle("Confirmation");
      
         alert.initModality(Modality.APPLICATION_MODAL);
         alert.setContentText("Veuillez renseigner un seul champs");
         		
         Button exitButton = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
         exitButton.setText("Oui");
         Optional<ButtonType> resu = alert.showAndWait();
    	return 0;
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	
    	Image img = new Image("assets/hrhead.jpg");
		imageHead.setImage(img);
        img = new Image("assets/home.png");
        HomeImg.setImage(img);
        img = new Image("assets/archive.png");
        cartonsImg.setImage(img);
        img = new Image("assets/send.png");
        demandesImg.setImage(img);
        img = new Image("assets/placeholder.png");
        sitesImg.setImage(img);
        img = new Image("assets/users.png");
        personnelImg.setImage(img);
        img = new Image("assets/projectmenu.png");
        statImg.setImage(img);
    	img = new Image("assets/archive.png");
		id_imagecarton.setImage(img);
		/*img = new Image("assets/search.png");
		id_imagerecherche.setImage(img);*/
		img = new Image("assets/20045.png");
		id_pluscarton.setImage(img);
		img = new Image("assets/info-.png");
		
 	    System.out.print("----------------------------------------------");
 	   List<Location> locs = new ArrayList<Location>();
		try {
			locs = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location/empty");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
        Collections.reverse(locs);
       for (Location location : locs ) {
        	    
        	   
    	   data_location.add(location);		
			  }
		
		/*for (Client client : clients) {
			System.out.print(client.getNom());
              data_nom_client.add(client.getNom());
		}*/
		List<Carton> cartons = new ArrayList<Carton>();
		try {
			cartons = JsonParserCarton.getCartonList("http://localhost:18080/Arka-web/api/carton");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	
         Collections.reverse(cartons);
        for (Carton carton : cartons ) {
         	    System.out.print(carton.getIdCarton());
         	   
         		data.add(carton);		
 			  }
           
        id_listecarton.setCellFactory(new Callback<ListView<Carton>, ListCell<Carton>>() {
			@Override
			public ListCell<Carton> call(ListView<Carton> args0) {
				Image img = new Image("assets/archive.png");
				ImageView imageview = new ImageView(img);
				Label label = new Label("");
				JFXButton buttinfo = new JFXButton();
				// butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill:
				// white;");
				buttinfo.setStyle("-fx-background-image: url('/assets/info-.png');-fx-font-size: 1em; ");
				buttinfo.setMinHeight(30);
				buttinfo.setMinWidth(30);
				
				
				JFXComboBox <Location>comboboxemplacement=new JFXComboBox(data_location);
				
				JFXButton buttAffecter = new JFXButton();
				buttAffecter.setText("affecter emplacement");
				buttAffecter.setStyle("-fx-background-color: #7f7fff;");
				buttAffecter.setMinHeight(30);
				buttAffecter.setMinWidth(50);
				//buttAffecter.setStyle("-fx-background-image: url('/assets/send.png');");
				buttAffecter.setAlignment(Pos.CENTER_RIGHT);
				HBox hbox = new HBox();
				hbox.getChildren().addAll(imageview, label, buttinfo,comboboxemplacement,buttAffecter);

				ListCell<Carton> cell;
				int i = 0;
				cell = new ListCell<Carton>() {

					@Override
					protected void updateItem(Carton r, boolean b) {

						super.updateItem(r, b);

						if (r != null) {
							buttinfo.setOnAction((event) -> {
								FXMLLoader loader = new FXMLLoader(getClass().getResource("InfoCarton.fxml"));

								Parent root;
								try {
									root = (Parent) loader.load();
									InfoCartonController ctrl = loader.getController();
									ctrl.setId_code(r.getIdCartonClient());
									ctrl.setId_dateentree(r.getArrivalDate());
									ctrl.setId_datedestruction(r.getDestructionDate());
									ctrl.setId_duree(r.getDuration());
									if(r.getClient()!=null)
									{
										ctrl.getId_affectclient().setStyle("-fx-background-color: #09e40d;");
										ctrl.getId_affectclient().setText("Affect�e");
									}
									else
									{
										ctrl.getId_affectclient().setStyle("-fx-background-color:#ff9999;");
										ctrl.getId_affectclient().setText("Non affect�e");
									}
									if(r.getLocale()!=null)
									{
										System.out.println(r.getLocale().getIdLocation());
										ctrl.getId_affectemplacement().setStyle("-fx-background-color: #09e40d;");
										ctrl.getId_affectemplacement().setText("Affect�e");
									}
									else
									{
										//System.out.println(r.getLocale().getIdLocation());
										ctrl.getId_affectemplacement().setStyle("-fx-background-color:#ff9999;");
										ctrl.getId_affectemplacement().setText("Non affect�e");
									}
									Scene newScene = new Scene(root);
									Stage newStage = new Stage();
									newStage.setTitle("Informations");
									newStage.setScene(newScene);
									newStage.show();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							});
							buttAffecter.setOnAction((event) -> {
								int idcarton=id_listecarton.getSelectionModel().getSelectedItem().getIdCarton();
								int idlocation=comboboxemplacement.getValue().getIdLocation();
								try {
									JsonParserCarton.add_Carton_to_location(idcarton, idlocation);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							});
							
							if (r.getLocale() == null) {

								hbox.setStyle("-fx-background-color:#ff9999;");
							} 
							 else {
								
								buttAffecter.setDisable(true);
								hbox.setStyle("-fx-background-color:#09e40d;");
								//hbox.setStyle("-fx-background-color:#bef67a;");

							}
							label.setText("Code carton client:" + r.getIdCartonClient()+" "+"Affect�e � "+r.getClient().getNom());
							
							setGraphic(hbox);
						}

					}

				};

				return cell;
			}

		});
        id_listecarton.setItems(data);
    }    
    
}
