/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import com.jfoenix.controls.JFXButton;

import arka.domain.Demand;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import org.apache.http.client.ClientProtocolException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import jsonrRsources.JsonParserAgent;
import jsonrRsources.JsonParserDemand;;
/**
 * FXML Controller class
 *
 * @author Asusu
 */
public class AjouteAgentController implements Initializable {
    @FXML
    private ImageView imageHead;
    @FXML
    private ImageView LogoutImg;
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private ImageView HomeImg;
    @FXML
    private ImageView personnelImg;
    @FXML
    private ImageView personnelImg1;
    @FXML
    private TextField matricule;
    @FXML
    private TextField nom;
    @FXML
    private TextField adress;
    @FXML
    private TextField ntel;
    @FXML
    private TextField email;
    @FXML
    private TextField mpasse;
    @FXML
    private Button ajouterAg;
    @FXML
    private JFXButton rechercheagentbtn;
    @FXML
    private JFXButton ajouteragentbtn;
    @FXML
    private JFXButton listagentbtn;
    @FXML
    private JFXButton profil;
  
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void homeAction(ActionEvent event) throws IOException {
          FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void logoutAction(ActionEvent event) throws IOException {
         FXMLLoader loader=new FXMLLoader(getClass().getResource("Login.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
    stage.close();
    }


    @FXML
    private void ajouteragent(ActionEvent event) throws IOException {
       FXMLLoader loader=new FXMLLoader(getClass().getResource("AjouterAgent.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
        
        
    }

    @FXML
    private void rechercheagent(ActionEvent event) {
    }

    @FXML
    private void getlistagent(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("getlist.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
        
    }

    @FXML
    private void showProfil(ActionEvent event) throws IOException {
          FXMLLoader loader=new FXMLLoader(getClass().getResource("Profile.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void addagent(ActionEvent event) throws JAXBException {
 
		 try {
			
			 JsonParserAgent.add_Agent(matricule.getText(), nom.getText(),mpasse.getText(), email.getText(),adress.getText(), ntel.getText());
			 if(true){
				   Alert alert = new Alert(AlertType.INFORMATION);
				   alert.setTitle("Ajouter admin");
				   alert.setHeaderText(null);
				   alert.setContentText("votre noveau admin est enregistré");
				   alert.showAndWait();
				   matricule.setText("");
				   nom.setText("");
				   mpasse.setText("");
				   email.setText("");
				   adress.setText("");
				   ntel.setText("");}
		 
		 } catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }



}
