/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author Asusu
 */
public class AgentController implements Initializable {
    @FXML
    private ImageView imageHead;
    @FXML
    private ImageView LogoutImg;
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private JFXButton listagentbtn;
    @FXML
    private ImageView HomeImg;
    @FXML
    private ImageView personnelImg;
    @FXML
    private JFXButton ajouteragentbtn;
    @FXML
    private ImageView personnelImg1;
    @FXML
    private JFXButton rechercheagentbtn;
    @FXML
    private JFXButton profil;
    @FXML
    private Text idadminprofil;
    @FXML
    private Text nomprofil;
    @FXML
    private Text numtelprofile;
    @FXML
    private Text adressprofil;
    @FXML
    private Text emailprofil;

    @FXML
    private void homeAction(ActionEvent event) throws IOException {
       FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void logoutAction(ActionEvent event) throws IOException {
          FXMLLoader loader=new FXMLLoader(getClass().getResource("Login.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
    // do what you have to do
    stage.close();
    }

    @FXML
    private void getlistagent(ActionEvent event) throws IOException {
       FXMLLoader loader=new FXMLLoader(getClass().getResource("getlist.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void ajouteragent(ActionEvent event) throws IOException {
       FXMLLoader loader=new FXMLLoader(getClass().getResource("AjouteAgent.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void rechercheagent(ActionEvent event) {
    }

    @FXML
    private void showProfil(ActionEvent event) throws IOException {
       FXMLLoader loader=new FXMLLoader(getClass().getResource("Profile.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }

   
    
    
}
