/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import com.jfoenix.controls.JFXButton;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Asusu
 */
public class ClientController implements Initializable {
    @FXML
    private ImageView imageHead;
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private JFXButton listclientbtn;
    @FXML
    private ImageView HomeImg;
    @FXML
    private JFXButton ajouterclientbtn;
    @FXML
    private JFXButton profil;
    @FXML
    private ImageView personnelImg;
    @FXML
    private ImageView personnelImg1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void homeAction(ActionEvent event)throws IOException {
    	  FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
          Parent root=loader.load();
          Scene scene=new Scene(root);
          Stage  primaryStage= new Stage();
          primaryStage.setScene(scene);
          primaryStage.show();
          Stage stage = (Stage) imageHead.getScene().getWindow();
          stage.close();
    	
    }

    @FXML
    private void logoutAction(ActionEvent event )throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Login.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void getlistclient(ActionEvent event)throws IOException {
    	 FXMLLoader loader=new FXMLLoader(getClass().getResource("getlistclient.fxml"));
         Parent root=loader.load();
         Scene scene=new Scene(root);
         Stage  primaryStage= new Stage();
         primaryStage.setScene(scene);
         primaryStage.show();
         Stage stage = (Stage) imageHead.getScene().getWindow();
         stage.close();
    }

    @FXML
    private void ajouterclient(ActionEvent event)throws IOException {
    	 FXMLLoader loader=new FXMLLoader(getClass().getResource("ajouterclient.fxml"));
         Parent root=loader.load();
         Scene scene=new Scene(root);
         Stage  primaryStage= new Stage();
         primaryStage.setScene(scene);
         primaryStage.show();
         Stage stage = (Stage) imageHead.getScene().getWindow();
         stage.close();
    }

    @FXML
    private void showProfil(ActionEvent event)throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Profile.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }
    
}
