package fxml;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import jsonrRsources.JsonParserAuth;
import javafx.scene.text.Text;
/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class LoginController implements Initializable {

    @FXML
    private ImageView Dpimage;
    @FXML
    private JFXPasswordField passWordText;
    @FXML
    private JFXTextField emailText;
    @FXML
    private ImageView CompanyImage;
    @FXML
    private JFXButton LoginBtn;
    @FXML
    private Text error;
    
    String role;
    public static int id;
    /**
     * Initializes the controller class.
     */
    
    public void initialize(URL url, ResourceBundle rb) {
       Image img = new Image("assets/company.jpg");
      CompanyImage.setImage(img);
      img = new Image("https://intra.captb.fr/wp-content/uploads/2017/07/logo2.png");
      Dpimage.setImage(img);
    }
    
    @FXML
    public void loginAction() throws IOException{
        

   	 String password=passWordText.getText();
		 
   	 String email=emailText.getText();
      try {
     role = JsonParserAuth.auth("http://localhost:18080/Arka-web/api/auth/"+email+"/"+password);
     id=JsonParserAuth.id("http://localhost:18080/Arka-web/api/auth/getid/"+email+"/"+password);
      System.out.println(id+role);  

        if(role.equals("agent")){
       	 FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAgent.fxml"));
       	 Parent root=loader.load();
       	  Scene scene=new Scene(root);
             Stage  primaryStage= new Stage();
             primaryStage.setScene(scene);
             primaryStage.show();
              Stage stage = (Stage) Dpimage.getScene().getWindow();
          stage.close(); 
        }
        else{
       	 if(role.equals("admin")){
           	 FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
           	 Parent root=loader.load();
           	 Scene scene=new Scene(root);
                Stage  primaryStage= new Stage();
                primaryStage.setScene(scene);
                primaryStage.show();
                Stage stage = (Stage) Dpimage.getScene().getWindow();
                stage.close(); 
         
            }
       	 else{
       		 error.setText("erreur");
       	 }
        }
  
   }
      catch(Exception e){
   	   
      }

    }

  
    
}
