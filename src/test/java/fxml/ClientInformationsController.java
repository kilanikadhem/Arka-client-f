/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fxml;

import com.jfoenix.controls.JFXButton;

import arka.domain.Client;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class ClientInformationsController implements Initializable {

    @FXML
    private JFXButton listDemands;
    @FXML
    private JFXButton CancelBtn;
    @FXML
    private Text nomText;
    @FXML
    private Text RespText;
    @FXML
    private Text TelTxt;
    @FXML
    private Text mailtext;
    @FXML
    private Text adresseTxt;
    @FXML
    private Text nbneCartonsTxt;
    @FXML
    private ImageView  ClientImage;
 
    public static int c;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	 Image img = new Image("assets/clientInfo.png");
    	 ClientImage.setImage(img);
//    	 root = (Parent) loader.load();
//			ClientInformationsController ctrl = loader.getController();
//           ctrl.setAdresse(r.getClient().getAdress());
//           ctrl.setnomText(r.getClient().getNom());
//           ctrl.setmailtext(r.getClient().getEmail());
//           ctrl.setTelTxt(r.getClient().getNumTel());
//           ctrl.setRespText(r.getClient().getNomResp());
//           Scene newScene = new Scene(root);
//           Stage newStage = new Stage();
//           newStage.setTitle("Informations");
//           newStage.setScene(newScene);
//           newStage.show();
    	 
//    	 listDemands.setOnAction((event) -> {
//   		  FXMLLoader loader = new FXMLLoader(getClass().getResource("ListDemands.fxml"));
//   		  
//   		 Parent root;
//			try {
//				root = (Parent) loader.load();
//				ListDemandsController ctrl = loader.getController();
//               ctrl.setclientTxt(nomText.getText());
//               System.out.println("kadhemmmm");
//               System.out.println(c);
//              ctrl.setClient(c);
//               Scene newScene = new Scene(root);
//               Stage newStage = new Stage();
//               newStage.setTitle("Informations");
//               newStage.setScene(newScene);
//               newStage.show();
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//    	 
//    	 });
    }
    @FXML
    private void listDemandsAction(ActionEvent event) throws IOException {
       
 FXMLLoader loader = new FXMLLoader(getClass().getResource("ListDemands.fxml"));
  		  
   		 Parent root;
			try {
				root = (Parent) loader.load();
				ListDemandsController ctrl = loader.getController();
               ctrl.setclientTxt(nomText.getText());
               
               System.out.println("kadhemmmm");
               System.out.println(c);
              ctrl.setClient(c);
              
               Scene newScene = new Scene(root);
               Stage newStage = new Stage();
               newStage.setTitle("Informations");
               newStage.setScene(newScene);
               newStage.show();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }

    @FXML
    private void cancelAction(ActionEvent event) {
    	
           Stage stage = (Stage) listDemands.getScene().getWindow();
       // do what you have to do
       stage.close();
    }
	public void setAdresse(String txt){
		adresseTxt.setText(txt);
		
	}
	
	public void setRespText(String txt){
		RespText.setText(txt);
		
	}
	
	public void setTelTxt(String txt){
		TelTxt.setText(txt);
		
	}
    

	public void setnomText(String txt){
		nomText.setText(txt);
		
	}
	
	public void setmailtext(String txt){
		mailtext.setText(txt);	
	}
	
	public void setNbreDemands(String txt){
		mailtext.setText(txt);	
	}
	
	public void setCleint(int client){
		c=client;	
	}
    
    
}
