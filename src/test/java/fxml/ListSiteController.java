/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.http.client.ClientProtocolException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;

import arka.domain.Carton;
import arka.domain.Location;
import arka.domain.Site;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import jsonrRsources.JsonParserCarton;
import jsonrRsources.JsonParserEmplacement;
import jsonrRsources.JsonParserSite;

/**
 * FXML Controller class
 *
 * @author hafedh
 */
public class ListSiteController implements Initializable {

    /**
     * Initializes the controller class.
     */
	@FXML
    private ImageView imageHead;
	@FXML
    private Text id_imagecaptb;
	@FXML
    private ImageView id_imagecarton;
	@FXML
    private ImageView cartonsImg;
	
	@FXML
    private ImageView id_imagerecherche;
	@FXML
    private ImageView HomeImg;
	@FXML
    private ImageView demandesImg;
	@FXML
    private ImageView sitesImg;
	@FXML
    private ImageView personnelImg;
	@FXML
    private ImageView id_pluscarton;
	@FXML
    private ImageView id_pluscarton1;
	@FXML
    private ImageView statImg;
	@FXML
    private JFXTextField id_nomsite;
	@FXML
    private JFXButton id_recherchesite;
	@FXML
    private JFXButton idajouter;
	@FXML
    private JFXButton idajouter1;
	@FXML
    private JFXListView<Site> id_listesite;
	@FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton CartonBtn;
    @FXML
    private JFXButton DemandesBtn;
    @FXML
    private JFXButton SitesBtn;
    
    @FXML
    private JFXButton PersonnelBtn;
	
	private ObservableList<Site> data=FXCollections.observableArrayList();
	private ObservableList<Location> data_location=FXCollections.observableArrayList();
	@FXML
    private void homeAction(ActionEvent event) throws IOException {
       FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void cartonsBtn(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListeCarton.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void demandesAction(ActionEvent event) {
    }

    @FXML
    private void sitesActions(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListSite.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }
    @FXML
    private void personnelAction(ActionEvent event) {
    }
    @FXML
    private void ajouterSite(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("SiteMngmt.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }
    @FXML
    private void ajouterEmplacement(ActionEvent event) throws IOException {
    	int id=id_listesite.getSelectionModel().getSelectedItem().getIdSite();
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Emplacement.fxml"));
        Parent root=loader.load();
        EmplacementController controller = loader.getController();
        controller.setIdsite(id);
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }
    @FXML
    private void Recherchersite(ActionEvent event) throws IOException {
    	 ObservableList<Site> data=FXCollections.observableArrayList();
    	 ObservableList<Location> data_location=FXCollections.observableArrayList();
    	id_listesite.setItems(data);
    	String nom=id_nomsite.getText();
    	List<Site> sites = new ArrayList<Site>();
		try {
			System.out.println("http://localhost:18080/Arka-web/api/site/"+nom);
			sites = JsonParserSite.getSiteList("http://localhost:18080/Arka-web/api/site/"+nom);
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	
         Collections.reverse(sites);
        for (Site site : sites ) {
         	    System.out.print(site.getIdSite());
         	   
         		data.add(site);		
 			  }
        
        
        id_listesite.setCellFactory(new Callback<ListView<Site>, ListCell<Site>>() {
			@Override
			public ListCell<Site> call(ListView<Site> args0) {
				Image img = new Image("assets/placeholder.png");
				ImageView imageview = new ImageView(img);
				Label label = new Label("");
				JFXButton buttinfo = new JFXButton();
				buttinfo.setText("Voir emplacement");
				buttinfo.setStyle("-fx-background-color: #7f7fff;");
				buttinfo.setMinHeight(30);
				buttinfo.setMinWidth(30);
				
				
				HBox hbox = new HBox();
				hbox.getChildren().addAll(imageview, label,buttinfo);

				ListCell<Site> cell;
				int i = 0;
				cell = new ListCell<Site>() {

					@Override
					protected void updateItem(Site r, boolean b) {

						super.updateItem(r, b);

						if (r != null) {
							
							hbox.setStyle("-fx-background-color:#fda36f;");
								//hbox.setStyle("-fx-background-color:#bef67a;");

							
							label.setText("Nom :" + r.getName()+" "+"Addresse :"+ r.getAddress() );
							
							setGraphic(hbox);
                                buttinfo.setOnAction((event) -> {
								
								//System.out.println(boxlocation.getValue().getIdLocation());
								
								FXMLLoader loader = new FXMLLoader(getClass().getResource("ListEmplacement.fxml"));
								int idsite=id_listesite.getSelectionModel().getSelectedItem().getIdSite();
								System.out.println("----------------------------"+idsite);
								Parent root;
								try {
									
									root = (Parent) loader.load();
									ListEmplacementController controller = loader.getController();
									
									
									//ListEmplacementController.setIdsite(idsite);
									Scene newScene = new Scene(root);
									Stage newStage = new Stage();
									newStage.setTitle("Emplacement");
									newStage.setScene(newScene);
									newStage.show();
									controller.setIdsit(idsite);
									
									ObservableList<Location> locations_lst=FXCollections.observableArrayList();
									List<Location> locatins = new ArrayList<Location>();
									try {
										
										locatins = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location/site/"+controller.getIdsit());
										
									} catch (ClientProtocolException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
							 	
							         Collections.reverse(locatins);
							        for (Location location : locatins ) {
							         	    
							         	   
							        	locations_lst.add(location);		
							 			  }
								    
							        controller.getId_alleecolumn().setCellValueFactory(new PropertyValueFactory<Location,Integer>("line"));
							        controller.getId_etagecolumn().setCellValueFactory(new PropertyValueFactory<Location,Integer>("row"));
							        controller.getId_colonnecolumn().setCellValueFactory(new PropertyValueFactory<Location,Integer>("driveway"));
							        controller.getId_videcolumn().setCellValueFactory(new PropertyValueFactory<Location,Boolean>("empty"));
							        controller.setIdsit(controller.getId_treetable().getSelectionModel().getSelectedItem().getSite().getIdSite());
							        controller.getId_treetable().setItems(locations_lst);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							});
						}

					}

				};

				return cell;
			}

		});
        id_listesite.setItems(data);
    }    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	Image img = new Image("assets/hrhead.jpg");
		imageHead.setImage(img);
        img = new Image("assets/home.png");
        HomeImg.setImage(img);
        img = new Image("assets/archive.png");
        cartonsImg.setImage(img);
        img = new Image("assets/send.png");
        demandesImg.setImage(img);
        img = new Image("assets/placeholder.png");
        sitesImg.setImage(img);
        img = new Image("assets/users.png");
        personnelImg.setImage(img);
        img = new Image("assets/projectmenu.png");
        statImg.setImage(img);
    	img = new Image("assets/placeholder.png");
		id_imagecarton.setImage(img);
		img = new Image("assets/20045.png");
		id_pluscarton.setImage(img);
		img = new Image("assets/20045.png");
		id_pluscarton1.setImage(img);
		List<Site> sites = new ArrayList<Site>();
		try {
			sites = JsonParserSite.getSiteList("http://localhost:18080/Arka-web/api/site");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	
         Collections.reverse(sites);
        for (Site site : sites ) {
         	    System.out.print(site.getIdSite());
         	   
         		data.add(site);		
 			  }
        
        List<Location> locations = new ArrayList<Location>();
		try {
			locations = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	
         Collections.reverse(locations);
        for (Location location : locations ) {
         	    System.out.print(location.getIdLocation());
         	   
         	   data_location.add(location);		
 			  }
        id_listesite.setCellFactory(new Callback<ListView<Site>, ListCell<Site>>() {
			@Override
			public ListCell<Site> call(ListView<Site> args0) {
				Image img = new Image("assets/placeholder.png");
				ImageView imageview = new ImageView(img);
				Label label = new Label("");
				JFXButton buttinfo = new JFXButton();
				buttinfo.setText("Voir emplacement");
				buttinfo.setStyle("-fx-background-color: #7f7fff;");
				buttinfo.setMinHeight(30);
				buttinfo.setMinWidth(30);
				
				
				HBox hbox = new HBox();
				hbox.getChildren().addAll(imageview, label,buttinfo);

				ListCell<Site> cell;
				int i = 0;
				cell = new ListCell<Site>() {

					@Override
					protected void updateItem(Site r, boolean b) {

						super.updateItem(r, b);

						if (r != null) {
							/*buttlocation.setOnAction((event) -> {
								
								System.out.println(boxlocation.getValue().getIdLocation());
								
									try {
										JsonParserSite.affect_location_site(boxlocation.getValue().getIdLocation(), r.getIdSite());
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									

							});*/
							
							hbox.setStyle("-fx-background-color:#fda36f;");
								//hbox.setStyle("-fx-background-color:#bef67a;");

							
							label.setText("Nom :" + r.getName()+" "+"Addresse :"+ r.getAddress() );
							
							
							buttinfo.setOnAction((event) -> {
								
								//System.out.println(boxlocation.getValue().getIdLocation());
								
								FXMLLoader loader = new FXMLLoader(getClass().getResource("ListEmplacement.fxml"));
								int idsite=id_listesite.getSelectionModel().getSelectedItem().getIdSite();
								System.out.println("----------------------------"+idsite);
								Parent root;
								try {
									
									root = (Parent) loader.load();
									ListEmplacementController controller = loader.getController();
									
									
									//ListEmplacementController.setIdsite(idsite);
									Scene newScene = new Scene(root);
									Stage newStage = new Stage();
									newStage.setTitle("Emplacement");
									newStage.setScene(newScene);
									newStage.show();
									controller.setIdsit(idsite);
									
									ObservableList<Location> locations_lst=FXCollections.observableArrayList();
									List<Location> locatins = new ArrayList<Location>();
									try {
										
										locatins = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location/site/"+controller.getIdsit());
										
									} catch (ClientProtocolException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
							 	
							         Collections.reverse(locatins);
							        for (Location location : locatins ) {
							         	    
							         	   
							        	locations_lst.add(location);		
							 			  }
								    
							        controller.getId_alleecolumn().setCellValueFactory(new PropertyValueFactory<Location,Integer>("line"));
							        controller.getId_etagecolumn().setCellValueFactory(new PropertyValueFactory<Location,Integer>("row"));
							        controller.getId_colonnecolumn().setCellValueFactory(new PropertyValueFactory<Location,Integer>("driveway"));
							        controller.getId_videcolumn().setCellValueFactory(new PropertyValueFactory<Location,Boolean>("empty"));
									
							        controller.getId_treetable().setItems(locations_lst);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							});
							setGraphic(hbox);
						}

					}

				};

				return cell;
			}

		});
        id_listesite.setItems(data);
    }    
    
}
