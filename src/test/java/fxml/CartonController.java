/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import javafx.geometry.Pos;
import arka.domain.*;
import java.io.IOException;
import java.net.URL;
import java.nio.file.attribute.PosixFilePermission;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import javafx.util.Callback;

import javax.xml.bind.JAXBException;
import javax.xml.crypto.Data;

import org.apache.http.client.ClientProtocolException;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import jsonrRsources.JsonParserCarton;
import jsonrRsources.JsonParserDemand;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author hafedh
 */
public class CartonController implements Initializable {

    /**
     * Initializes the controller class.
     */
	@FXML
    private JFXTextField id_codecarton;
	@FXML
    private JFXTextField id_duree;
	@FXML
    private JFXDatePicker iddateentree;
	@FXML
    private JFXDatePicker iddatedestruction;
	@FXML
    private JFXButton idajouter;
	@FXML
    private ImageView imageHead;
	@FXML
    private ImageView cartonsImg;
	
	@FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton CartonBtn;
    @FXML
    private JFXButton DemandesBtn;
    @FXML
    private JFXButton SitesBtn;
    
    @FXML
    private JFXButton PersonnelBtn;
	
	@FXML
    private ImageView HomeImg;
	@FXML
    private ImageView demandesImg;
	@FXML
    private ImageView sitesImg;
	@FXML
    private ImageView personnelImg;
	@FXML
    private ImageView c;
	@FXML
    private ImageView statImg;
	@FXML
    private ImageView id_plus;
	@FXML
    private ImageView id_imgdate;
	@FXML
    private ImageView id_pluscarton;
	@FXML
    private ImageView id_imagecarton;
	private ObservableList<Client> data_client=FXCollections.observableArrayList();
	@FXML
    private JFXComboBox <Client> comboclient;    
	
    @FXML
    private void ajouterCarton(ActionEvent event) throws IOException, JAXBException {
    	
    	int a=Integer.parseInt(id_codecarton.getText());
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    	String b=iddateentree.getValue().format(formatter);
    	DateTimeFormatter formater = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    	String c=iddatedestruction.getValue().format(formater);
		
		long i=Long.parseLong(id_duree.getText());
		int d=comboclient.getValue().getIdClient();
		JsonParserCarton.add_Carton(d,a,b,c,i);
		System.out.println("------------------------------------------");
    }
    @FXML
    private void homeAction(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void cartonsBtn(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListeCarton.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void demandesAction(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdminDemand.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void sitesActions(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListSite.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }
    @FXML
    private void personnelAction(ActionEvent event) {
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	Image img = new Image("assets/hrhead.jpg");
		imageHead.setImage(img);
        img = new Image("assets/home.png");
        HomeImg.setImage(img);
        
        img = new Image("assets/send.png");
        demandesImg.setImage(img);
        img = new Image("assets/placeholder.png");
        sitesImg.setImage(img);
        img = new Image("assets/users.png");
        personnelImg.setImage(img);
        img = new Image("assets/projectmenu.png");
        statImg.setImage(img);
    	
		img = new Image("assets/archive.png");
		cartonsImg.setImage(img);
		img = new Image("assets/document-add-icon.png");
		id_plus.setImage(img);
		img = new Image("assets/20045.png");
		id_pluscarton.setImage(img);
		img = new Image("assets/original_personalised-calendar-date-card.jpg");
		id_imgdate.setImage(img);
		img = new Image("assets/archive.png");
		id_imagecarton.setImage(img);
		List<Client> clients = new ArrayList<Client>();
		try {
			clients = JsonParserCarton.getClientList("http://localhost:18080/Arka-web/api/carton/clients");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Collections.reverse(clients);
		for (Client client : clients) {
			System.out.print(client.getIdClient());

			data_client.add(client);
		}
		comboclient.setItems(data_client);
    }    
    
}
