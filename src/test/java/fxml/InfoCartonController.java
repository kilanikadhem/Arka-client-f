/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

import javax.swing.plaf.basic.BasicBorders.ToggleButtonBorder;

import com.jfoenix.controls.JFXToggleButton;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author hafedh
 */
public class InfoCartonController implements Initializable {

    /**
     * Initializes the controller class.
     */
	@FXML
    private Text id_code;
	@FXML
    private Text id_dateentree;
	@FXML
    private Text id_datedestruction;
	@FXML
    private Text id_duree;
	@FXML
    private ImageView id_imagearchive;
	@FXML
    private ImageView imageHead;
	@FXML
    private Label id_affectclient;
	@FXML
    private Label id_affectemplacement;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	Image img = new Image("assets/archive.png");
    	id_imagearchive.setImage(img);
    }
    @FXML
    public void Retouraulistecarton() throws IOException
    {
    	
        Stage stage = (Stage) id_imagearchive.getScene().getWindow();
        stage.close();
    }
    
    public Text getId_code() {
		return id_code;
	}
	public void setId_code(int id_code) {
		
		this.id_code.setText(""+id_code);
	}
	public Text getId_dateentree() {
		return id_dateentree;
	}
	public void setId_dateentree(Date id_dateentree) {
		this.id_dateentree.setText(""+id_dateentree);
	}
	public Text getId_datedestruction() {
		return id_datedestruction;
	}
	public void setId_datedestruction(Date id_datedestruction) {
		this.id_datedestruction.setText(""+id_datedestruction);
	}
	public Text getId_duree() {
		return id_duree;
	}
	public void setId_duree(long id_duree) {
		this.id_duree.setText(""+id_duree);
	}
	public ImageView getId_imagearchive() {
		return id_imagearchive;
	}
	public void setId_imagearchive(ImageView id_imagearchive) {
		this.id_imagearchive = id_imagearchive;
	}
	public Label getId_affectclient() {
		return id_affectclient;
	}
	public void setId_affectclient(Label id_affectclient) {
		this.id_affectclient = id_affectclient;
	}
	public Label getId_affectemplacement() {
		return id_affectemplacement;
	}
	public void setId_affectemplacement(Label id_affectemplacement) {
		this.id_affectemplacement = id_affectemplacement;
	}
	  
    
}
