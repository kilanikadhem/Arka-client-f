/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;

import arka.domain.Agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;

import arka.domain.Client;
import arka.domain.Demand;
import arka.domain.DemandState;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;


import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.stage.Stage;
import jsonrRsources.JsonParserAgent;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import java.util.Optional;
/**
 * FXML Controller class
 *
 * @author Asusu
 */
public class GetlistController implements Initializable {
    @FXML
    private JFXListView<Agent> listagents;
    @FXML
    private ImageView imageHead;
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton ajouteragentbtn;
    @FXML
    private JFXButton listagentbtn;
    @FXML
    private JFXButton rechercheagentbtn;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private ImageView HomeImg;
    @FXML
    private ImageView personnelImg;
    @FXML
    private ImageView personnelImg1;
    @FXML
    private ImageView LogoutImg;

    /**
     * Initializes the controller class.
     */
    private ObservableList<Agent> data=FXCollections.observableArrayList();
    @Override
    public void initialize(URL url, ResourceBundle rb) {
                	data.clear();
    	    	    	 HBox hbox = new HBox();
    	    	    	List<Agent>agents = new ArrayList<Agent>();
    	    			try {
    	    				System.out.println("http://localhost:18080/Arka-web/api/user");
    	    				agents  = JsonParserAgent.getAgentList("http://localhost:18080/Arka-web/api/user");
    	    			} catch (ClientProtocolException e) {
    	    				// TODO Auto-generated catch block
    	    				e.printStackTrace();
    	    			} catch (IOException e) {
    	    				// TODO Auto-generated catch block
    	    				e.printStackTrace();
    	    			}
    	    			
    	    			for (Agent agent : agents) {
    	    				System.out.println (agent.getNom());	}
    	    	
    	    			Collections.reverse(agents);
    	    	        for (Agent agent : agents ) {
    	    	                System.out.print(agent.getMatricule());
    	    	               
    	    	                data.add(agent);       
    	    	             
    	    	         
    	    	         }
    	    	        System.out.println ("size est "+agents.size());
    	    	        listagents.setCellFactory(new Callback<ListView<Agent>, ListCell<Agent>>(){
    	    	            @Override
    	    	            public ListCell<Agent> call(ListView<Agent> args0) {
    	    	            	Image img= new Image("assets/file.png");
    	    	                ImageView imageview=new ImageView(img);
    	    	         
    	    	                Label label=new Label("");
    	    	                JFXButton buttdelete=new JFXButton();
    	    	                buttdelete.setStyle("-fx-background-image: url('/assets/delete.png');");
    	    	                buttdelete.setMinHeight(35);
    	    	                buttdelete.setMinWidth(35);
    	    	                buttdelete.setAlignment(Pos.CENTER_RIGHT);
    	    	               
    	    	                JFXButton emptybtn=new JFXButton();
    	    	                emptybtn.setMinHeight(35);
    	    	                emptybtn.setMinWidth(35);
    	    	                //emptybtn.setStyle("fx-background-image: url('/assets/delete.png');");
    	    	                //emptybtn.setDisable(true);
    	    	                emptybtn.setAlignment(Pos.CENTER_RIGHT);
    	    	               
    	    	                JFXButton buttProfil=new JFXButton();
    	    	                buttProfil.setStyle("-fx-background-image: url('/assets/send.png');");
    	    	                buttProfil.setMinHeight(35);
    	    	                buttProfil.setMinWidth(35);
    	    	               
    	    	                HBox hbox = new HBox();
    	    	                hbox.getChildren().addAll(imageview, label,buttdelete,buttProfil);
    	    	                ListCell<Agent> cell;
    	    	                int i = 0 ;
    	    	                cell = new ListCell<Agent>(){
    	    	                   
    	    	                	@Override
    	    	                    protected void updateItem(Agent r ,boolean b){
    	    	                      
    	    	                        super.updateItem(r,b);
    	    	                        
    	    	                          if(r != null){
    	    	                        	  buttdelete.setOnAction((event) -> {
    	    	                        		 
    	    	                        				
    	    	                        			try {
    	    	                        				
														Alert alert = new Alert(AlertType.CONFIRMATION);
														alert.setTitle("Confirmation Dialog");
														alert.setHeaderText("Look, a Confirmation Dialog");
														alert.setContentText("Are you ok with this?");

														Optional<ButtonType> result = alert.showAndWait();
														if (result.get() == ButtonType.OK){
															JsonParserAgent.DeleteAgent(r.getIdAgent());
															Stage stage = (Stage) imageHead.getScene().getWindow();
														     stage.close();
														     FXMLLoader loader=new FXMLLoader(getClass().getResource("Getlist.fxml"));
												             Parent root=loader.load();
												        Scene scene=new Scene(root);
												        Stage  primaryStage= new Stage();
												        primaryStage.setScene(scene);
												        primaryStage.show();
														}
													} catch (Exception e) {
														// TODO Auto-generated catch block
														e.printStackTrace();
													}
    	    	                        	    	
    	    	                        		  
    	    	                        		 
    	    	                        	  });
    	    	                              buttProfil.setOnAction((event) -> {
    	    	                                  FXMLLoader loader = new FXMLLoader(getClass().getResource("updateAgent.fxml"));
    	    	                                  
    	    	                                  Parent root;
    	    	                                  
    	    	                                try {
                                                  root = (Parent) loader.load();
    	                                          UpdateAgentController update= loader.getController();          
  	    	                                	  update.setid(r.getIdAgent());
    	                                          update.setnomtxt(r.getNom());
  	    	                                	  update.setemailtxt(r.getEmail());
  	    	                                	  update.setprenomtxt(r.getPrenom());
  	    	                                      update.setmatricule(r.getMatricule());
  	    	                                      update.setnumtel(r.getNumTel());
    	      	                                  Scene newScene = new Scene(root);
    	  	                                      Stage newStage = new Stage();
    	  	                                      newStage.setTitle("Informations");
    	  	                                      newStage.setScene(newScene);
    	  	                                      newStage.show();
    	  	                                    FXMLLoader loader2=new FXMLLoader(getClass().getResource("Getlist.fxml"));
    	  	                 	          
    	    	                                } catch (Exception e) {
    	    	                                    // TODO Auto-generated catch block
    	    	                                    e.printStackTrace();
    	    	                                }
    	    	                                  
    	    	                                  
    	    	                              });
    	    	                            
    	    	                            label.setText("Agent: " +r.getMatricule()+"   Nom: "+r.getNom() + "   Prenom: "+r.getPrenom()+ "   email: "+r.getEmail());
    	    	                            setGraphic(hbox);}
    	    
    	    	                        }
    	    	                      };
    	    	             return cell;    
    	    	            }
                                 });
    	    	        listagents.setItems(data);
    	    	        
    	    	    }
    	    	
        

    @FXML
    private void homeAction(ActionEvent event) throws IOException{
    	 FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
         Parent root=loader.load();
         Scene scene=new Scene(root);
         Stage  primaryStage= new Stage();
         primaryStage.setScene(scene);
         primaryStage.show();
         Stage stage = (Stage) imageHead.getScene().getWindow();
         stage.close();
    }

    @FXML
    private void ajouteragent(ActionEvent event)throws IOException {
    	 FXMLLoader loader=new FXMLLoader(getClass().getResource("AjouteAgent.fxml"));
         Parent root=loader.load();
         Scene scene=new Scene(root);
         Stage  primaryStage= new Stage();
         primaryStage.setScene(scene);
         primaryStage.show();
         Stage stage = (Stage) imageHead.getScene().getWindow();
         stage.close();
          
    }

    @FXML
    private void getlistagent(ActionEvent event)throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("getlist.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void rechercheagent(ActionEvent event)throws IOException {
    }

    @FXML
    private void logoutAction(ActionEvent event) throws IOException {
    	 FXMLLoader loader=new FXMLLoader(getClass().getResource("Login.fxml"));
         Parent root=loader.load();
         Scene scene=new Scene(root);
         Stage  primaryStage= new Stage();
         primaryStage.setScene(scene);
         primaryStage.show();
          Stage stage = (Stage) imageHead.getScene().getWindow();
      stage.close();
    }
    
}
