/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import com.jfoenix.controls.JFXTextField;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import org.apache.http.client.ClientProtocolException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import jsonrRsources.JsonParserAgent;

/**
 * FXML Controller class
 *
 * @author Asusu
 */
public class UpdateAgentController implements Initializable {
   
    @FXML
    private Button savebtn;
    @FXML
    private Button cancelbtn;
    @FXML
    private JFXTextField nomtxt;
    @FXML
    private JFXTextField adresstxt;
    @FXML
    private JFXTextField emailtxt;
    @FXML
    private  JFXTextField matriculetxt;
    @FXML
    private  JFXTextField numteltxt;
    @FXML
    private  JFXTextField idtxt;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	idtxt.setVisible(false);
       idtxt.setDisable(true);
    }
    
    @FXML
    private void saveupdate(ActionEvent event) throws JAXBException {
         try {
			 JsonParserAgent.upadateAgent( Integer.parseInt(idtxt.getText()),matriculetxt.getText(), nomtxt.getText(), emailtxt.getText(),adresstxt.getText(),numteltxt.getText());
		
			
			  Stage stage = (Stage) cancelbtn.getScene().getWindow();
			    stage.close();
			    
         } catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    private void cancelupdate(ActionEvent event) {
    	Stage stage = (Stage) cancelbtn.getScene().getWindow();
	    stage.close();
    }

    public void setnomtxt(String txt){
    	nomtxt.setText(txt);
    }
    
    public void setemailtxt(String txt){
    	emailtxt.setText(txt);
    }
    public void setprenomtxt(String txt) {
    	adresstxt.setText(txt);
		
	}
    
    public void setmatricule(String txt){
    	matriculetxt.setText(txt);
    }
    public void setid(int txt){
    	idtxt.setText(String.valueOf(txt));
    }
    public void setnumtel(String txt){
    	numteltxt.setText(txt);
    }
    
}
