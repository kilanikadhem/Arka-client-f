/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fxml;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;

import arka.domain.Demand;
import arka.domain.DemandState;
import arka.domain.DemandType;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import jsonrRsources.JsonParserDemand;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javax.xml.crypto.Data;

import org.apache.http.client.ClientProtocolException;
/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class AgentDemandPersoFXMLController implements Initializable {

    @FXML
    private ImageView imageHead;
    @FXML
    private ImageView LogoutImg;
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton CartonBtn;
    @FXML
    private JFXButton DemandesBtn;
    @FXML
    private JFXButton SitesBtn;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private JFXButton PersonnelBtn;
    @FXML
    private JFXButton PersonnelBtn1;
    @FXML
    private ImageView HomeImg;
    @FXML
    private ImageView cartonsImg;
    @FXML
    private ImageView demandesImg;
    @FXML
    private ImageView sitesImg;
    @FXML
    private ImageView personnelImg;
    @FXML
    private ImageView statImg;
    @FXML
    private JFXDatePicker datepicktxt;
    @FXML
    private JFXComboBox<DemandType> typeCombo;
    @FXML
    private JFXComboBox<DemandState> etatCombo;
    @FXML
    private JFXTextField ClientTxt;
    @FXML
    private ImageView imgSearch;
    @FXML
    private JFXButton search;
    @FXML
    private JFXListView<Demand> listViewDemands;
    
    private ObservableList<Demand> data=FXCollections.observableArrayList();
    @FXML
    private ImageView actualiserImg;
    @FXML
    private JFXButton refrechBtn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	Image img = new Image("assets/hrhead.jpg");
      	 imageHead.setImage(img);
           img = new Image("assets/logout.png");
           LogoutImg.setImage(img);
           img = new Image("assets/home.png");
           HomeImg.setImage(img);
           img = new Image("assets/archive.png");
           cartonsImg.setImage(img);
           img = new Image("assets/send.png");
           demandesImg.setImage(img);
           img = new Image("assets/placeholder.png");
           sitesImg.setImage(img);
           img = new Image("assets/users.png");
           personnelImg.setImage(img);
           img = new Image("assets/projectmenu.png");
           statImg.setImage(img);
           img = new Image("assets/search.png");
           imgSearch.setImage(img);
           img = new Image("assets/refresh.png");
           actualiserImg.setImage(img);
         
           typeCombo.getItems().addAll(DemandType.Add,DemandType.Retrait,DemandType.Expedition,DemandType.Divers);
           etatCombo.getItems().addAll(DemandState.nonTraitee,DemandState.affectee,DemandState.traitee);
           HBox hbox = new HBox();
           data.clear();
        	List<Demand> demands = new ArrayList<Demand>();
    		try {
    			System.out.println("http://localhost:18080/Arka-web/api/demandes/DemandsClient/"+ClientInformationsController.c);
    			demands = JsonParserDemand.getDemandListByAgent("http://localhost:18080/Arka-web/api/demandes/DemandsAgent","Arka1");
    		} catch (ClientProtocolException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		
    		  Collections.reverse(demands);
    	        for (Demand demand : demands ) {
    	         	    System.out.println(demand.getIdDemand());
    	         	   
    	         		data.add(demand);		
    	 			 
    	         
    	         }
    	        
    	        fillListView(demands);
    }    

    @FXML
    private void homeAction(ActionEvent event) {
    }

    @FXML
    private void cartonsBtn(ActionEvent event) {
    }

    @FXML
    private void demandesAction(ActionEvent event) {
    }

    @FXML
    private void sitesActions(ActionEvent event) {
    }

    @FXML
    private void logoutAction(ActionEvent event) {
    }

    @FXML
    private void personnelAction(ActionEvent event) {
    }

    @FXML
    private void searchAction(ActionEvent event) {
    }

    @FXML
    private void refrechAction(ActionEvent event) {
    }
    public void fillListView(List<Demand> demands){
    	data.clear();
	        
        Collections.reverse(demands);
        for (Demand demand : demands ) {
         	    System.out.print(demand.getIdDemand());
         	   
         		data.add(demand);		
 			 
         
         }
        listViewDemands.setCellFactory(new Callback<ListView<Demand>, ListCell<Demand>>(){
            @Override
            public ListCell<Demand> call(ListView<Demand> args0) {
                Image img= new Image("assets/file.png");
                ImageView imageview=new ImageView(img);
                Label label=new Label("");
                JFXButton buttClient=new JFXButton();
                //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
                //buttClient.setStyle("-fx-background-image: url('/assets/info.png');-fx-font-size: 1em; ");
                buttClient.setMinHeight(30);
                buttClient.setMinWidth(30);
                JFXButton buttProfil=new JFXButton();
               // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
               buttProfil.setMinHeight(30);
                buttProfil.setMinWidth(32);
                JFXButton buttAffecter=new JFXButton();
                
                buttAffecter.setMinHeight(30);
                buttAffecter.setMinWidth(30);
                buttAffecter.setStyle("-fx-background-image: url('/assets/checked.png');");
                buttAffecter.setAlignment(Pos.CENTER_RIGHT);
                JFXButton buttCartons=new JFXButton();
                buttCartons.setStyle("-fx-background-image: url('/assets/archive.png');");
                buttCartons.setMinHeight(30);
                buttCartons.setMinWidth(32);
                HBox hbox = new HBox();
                hbox.getChildren().addAll(imageview, label,buttProfil,buttClient,buttAffecter,buttCartons);
                
                
                
                ListCell<Demand> cell;
                int i = 0 ;
                cell = new ListCell<Demand>(){
                    
                    @Override
                    protected void updateItem(Demand r ,boolean b){
                 	  
                        super.updateItem(r,b);
                        
                          if(r != null){
                        	  buttCartons.setOnAction((event)->{
                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("cartonListFXML.fxml"));
                        		  Parent root;
                        		  try {
                        			  
									root = (Parent) loader.load();
									CartonListFXMLController clfc= loader.getController();
									clfc.setCLientName(r.getClient().getNom());
									clfc.setDate(r.getDate()+"");
									System.out.println("sizee = "+r.getCartons().size());
									clfc.fillListCartons(r.getCartons());
									  Scene newScene = new Scene(root);
	                                  Stage newStage = new Stage();
	                                  newStage.setTitle("Informations");
	                                  newStage.setScene(newScene);
	                                  newStage.show();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
									  
                        		  
                        	  });
                        	  buttAffecter.setOnAction((event) -> {
                        		  Alert alert = new Alert(AlertType.CONFIRMATION);
                        		  alert.setTitle("Confirmation Dialog");
                        		  alert.setHeaderText("Confirmation de cette tache ");
                        		  alert.setContentText("un mail sera envoyer ");

                        		  Optional<ButtonType> result = alert.showAndWait();
                        		  if (result.get() == ButtonType.OK){
                        			  try {
										JsonParserDemand.changeDemandState("http://localhost:18080/Arka-web/api/demandes/changeDemandState",r.getIdDemand());
										  FXMLLoader loader=new FXMLLoader(getClass().getResource("AgentDemandPersoFXML.fxml"));
									       Parent root=loader.load();
									       Scene scene=new Scene(root);
									       Stage  primaryStage= new Stage();
									       primaryStage.setScene(scene);
									       primaryStage.show();
									       Stage stage = (Stage) imageHead.getScene().getWindow();
									       stage.close();
                        			  } catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
                        		  } else {
                        		      // ... user chose CANCEL or closed the dialog
                        		  }
                        		  
                        	  });
                         	 if(r.getDemandState()==DemandState.nonTraitee){
                         		
                         		 hbox.setStyle("-fx-background-color:#ff9999;");
                       	   }else if(r.getDemandState()==DemandState.affectee){
                       		buttAffecter.setDisable(true);
                       		 hbox.setStyle("-fx-background-color:#fda36f;");
                       	   }else{
                       		hbox.setStyle("-fx-background-color:#bef67a;");
                       		   
                       	   }
                            label.setText("Client:" +r.getClient().getNom()+" Nom :"+r.getClient().getNomResp() + " Type: "+r.getDemandType()+ " Date :"+r.getDate());
                            setGraphic(hbox);}
                            
                          
                          
                       
                              
                            
                           
                            
                             
                            
                            
                        
                     
                        }
                        
                      
                 
                };
                
                
             return cell;    
            }
             
           
        
                 });
        listViewDemands.setItems(data);
    	
    }
}
