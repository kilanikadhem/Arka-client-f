/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.apache.http.client.ClientProtocolException;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

import arka.domain.Carton;
import arka.domain.Location;
import arka.domain.Site;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import jsonrRsources.JsonParserEmplacement;
import jsonrRsources.JsonParserSite;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.beans.value.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.beans.property.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
/**
 * FXML Controller class
 *
 * @author hafedh
 */
public class ListEmplacementController implements Initializable {

    /**
     * Initializes the controller class.
     */
	
    private int idsit;
	@FXML
    private ImageView imageHead;
	@FXML
    private Text id_imagecaptb;
	@FXML
    private ImageView id_imagecarton;
	@FXML
    private ImageView cartonsImg;
	
	@FXML
    private ImageView id_imagerecherche;
	@FXML
    private ImageView HomeImg;
	@FXML
    private ImageView demandesImg;
	@FXML
    private ImageView sitesImg;
	@FXML
    private ImageView personnelImg;
	
	@FXML
    private ImageView statImg;
	@FXML
    private JFXTextField id_codecarton;
	@FXML
    private JFXTextField id_codeclient;
	@FXML
    private JFXDatePicker id_dateentree;
	@FXML
    private JFXTextField id_allee;
	@FXML
    private JFXButton id_rechercher;
	@FXML
    private JFXButton id_rechercher1;
	@FXML
    private TableView <Location> id_treetable;
	@FXML
    private TableColumn <Location,Integer> id_alleecolumn;
	@FXML
    private TableColumn <Location,Integer>id_etagecolumn;
	@FXML
    private TableColumn <Location,Integer> id_colonnecolumn;
	@FXML
    private TableColumn <Location,Boolean> id_videcolumn;
	@FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton CartonBtn;
    @FXML
    private JFXButton DemandesBtn;
    @FXML
    private JFXButton SitesBtn;
    
    @FXML
    private JFXButton PersonnelBtn;
	private ObservableList<Location> locations=FXCollections.observableArrayList();
	private ObservableList<Location> locations_rech=FXCollections.observableArrayList();
	private ObservableList<Location> list1=FXCollections.observableArrayList();
	private ObservableList<Location> list2=FXCollections.observableArrayList();
	private ObservableList<Location> list3=FXCollections.observableArrayList();
	private ObservableList<Location> list4=FXCollections.observableArrayList();
	private ObservableList<Location> locations_rech_carton=FXCollections.observableArrayList();
	private final ObservableList<String> critere = FXCollections.observableArrayList("All�e", "Etage", "Colonne");
    
	@Override
    public void initialize(URL url, ResourceBundle rb) {
    	
    	Image img = new Image("assets/hrhead.jpg");
		imageHead.setImage(img);
        img = new Image("assets/home.png");
        HomeImg.setImage(img);
        img = new Image("assets/archive.png");
        cartonsImg.setImage(img);
        img = new Image("assets/send.png");
        demandesImg.setImage(img);
        img = new Image("assets/placeholder.png");
        sitesImg.setImage(img);
        img = new Image("assets/users.png");
        personnelImg.setImage(img);
        img = new Image("assets/projectmenu.png");
        statImg.setImage(img);
    	img = new Image("assets/placeholder.png");
		id_imagecarton.setImage(img);
		
		
		
		System.out.println(":::::::::::::::::::::::::::"+getIdsit()+":::::::::::::::::::::::::::");
		//id_treetable.getItems().clear();
		//System.out.println(":::::::::::::::::::::::::"+Integer.parseInt(idsit.getText()));
		/*List<Location> locatins = new ArrayList<Location>();
		try {
			
			locatins = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location/site/"+getIdsit());
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	
         Collections.reverse(locatins);
        for (Location location : locatins ) {
         	    
         	   
         		locations.add(location);		
 			  }
	    
		id_alleecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("line"));
		id_etagecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("row"));
		id_colonnecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("driveway"));
		id_videcolumn.setCellValueFactory(new PropertyValueFactory<Location,Boolean>("empty"));
		
		  id_treetable.setItems(locations);*/
		
    }    
    @FXML
    private void homeAction(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void cartonsBtn(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListeCarton.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void demandesAction(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdminDemand.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void sitesActions(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListSite.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void personnelAction(ActionEvent event) {
    }
    @FXML
    private int Rechercheremplacement_carton(ActionEvent event) throws IOException {
    	
    	id_treetable.getItems().clear();
    	
    	//ClientTxt.getText().equals("")==false
	    if((id_codecarton.getText().equals("")==false)&&id_codeclient.getText().equals("")&&id_dateentree.getValue()==null)
	    {
	    	System.out.println("-------------------------yes");
	    	System.out.println("-----"+id_codeclient.getText());
	    	
	    		System.out.println("--------------------------no");
	    		List<Location> lst1 = new ArrayList<Location>();
				try {
					
					lst1 = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location/rech1/"+getIdsit()+"/"+id_codecarton.getText());
					
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 	
		         Collections.reverse(lst1);
		        for (Location location : lst1 ) {
		         	    
		         	   
		        	list1.add(location);		
		 			  }
		        id_alleecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("line"));
				id_etagecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("row"));
				id_colonnecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("driveway"));
				id_videcolumn.setCellValueFactory(new PropertyValueFactory<Location,Boolean>("empty"));
				
				id_treetable.setItems(list1);
				//id_treetable
				return 0;
	    	
	    	}
	    if((id_codecarton.getText().equals(""))&&id_codeclient.getText().equals("")==false &&id_dateentree.getValue()==null)
    	{
	    	System.out.println("---------------------------------------");
    		List<Location> lst2 = new ArrayList<Location>();
			try {
				
				lst2 = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location/rech2/"+getIdsit()+"/"+id_codeclient.getText());
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 	
	         Collections.reverse(lst2);
	        for (Location location : lst2 ) {
	         	    
	         	   
	        	list2.add(location);		
	 			  }
	        id_alleecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("line"));
			id_etagecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("row"));
			id_colonnecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("driveway"));
			id_videcolumn.setCellValueFactory(new PropertyValueFactory<Location,Boolean>("empty"));
			
			id_treetable.setItems(list2);	
			return 0;
    	}
	    
	    if((id_codecarton.getText().equals(""))&&id_codeclient.getText().equals("")&&id_dateentree.getValue()!=null)
	    {
	    	LocalDate localDate = id_dateentree.getValue();//For reference
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	    	String formattedString = localDate.format(formatter);
	    	System.out.println("===================================="+formattedString);
	    	List<Location> lst3 = new ArrayList<Location>();
			try {
				
				lst3 = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location/rech3/"+getIdsit()+"/"+formattedString);
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 	
	         Collections.reverse(lst3);
	        for (Location location : lst3 ) {
	         	    
	         	   
	        	list3.add(location);		
	 			  }
	        id_alleecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("line"));
			id_etagecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("row"));
			id_colonnecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("driveway"));
			id_videcolumn.setCellValueFactory(new PropertyValueFactory<Location,Boolean>("empty"));
			
			id_treetable.setItems(list3);
			return 0;
	    }
	    if((id_codecarton.getText().equals("")==false)&&id_codeclient.getText().equals("")==false&&id_dateentree.getValue()!=null)
	    {
	    	LocalDate localDate = id_dateentree.getValue();//For reference
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	    	String formattedString = localDate.format(formatter);
	    	List<Location> lst4 = new ArrayList<Location>();
			try {
				
				lst4 = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location/"+getIdsit()+"/"+id_codecarton.getText()+"/"+id_codeclient.getText()+"/"+formattedString);
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 	
	         Collections.reverse(lst4);
	        for (Location location : lst4 ) {
	         	    
	         	   
	        	list4.add(location);		
	 			  }
	        id_alleecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("line"));
			id_etagecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("row"));
			id_colonnecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("driveway"));
			id_videcolumn.setCellValueFactory(new PropertyValueFactory<Location,Boolean>("empty"));
			
			id_treetable.setItems(list4);
			return 0;
	    }
	    
	    	System.out.println("------------------------------------------out");
	    	 Alert alert = new Alert(Alert.AlertType.ERROR);
	         alert.setHeaderText("CAPTB");
	         alert.setTitle("Confirmation");
	      
	         alert.initModality(Modality.APPLICATION_MODAL);
	         alert.setContentText("Veuillez renseigner tous les champs ou un seul "
	         		);
	         Button exitButton = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
	         exitButton.setText("Oui");
	         Optional<ButtonType> resu = alert.showAndWait();

	         return 0;
		
    }
    @FXML
    private void Rechercheremplacement_allee(ActionEvent event) throws IOException {
    	id_treetable.getItems().clear();
    	List<Location> emplacements = new ArrayList<Location>();
		try {
			
			emplacements = JsonParserEmplacement.getLocationList("http://localhost:18080/Arka-web/api/location/site/"+getIdsit()+"/"+id_allee.getText());
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	
         Collections.reverse(emplacements);
        for (Location location : emplacements ) {
         	    
         	   
         		locations_rech.add(location);		
 			  }
	    
		id_alleecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("line"));
		id_etagecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("row"));
		id_colonnecolumn.setCellValueFactory(new PropertyValueFactory<Location,Integer>("driveway"));
		id_videcolumn.setCellValueFactory(new PropertyValueFactory<Location,Boolean>("empty"));
		/*id_videcolumn.setCellFactory(column -> {
		        return new TableCell<Location,Integer>() {
		            @Override
		            protected void updateItem(Location item, boolean empty) {
		                super.updateItem(item, empty);

		                setText(empty ? "" : getItem().toString());
		                setGraphic(null);

		                TableRow<CallLogs> currentRow = getTableRow();

		                if (!isEmpty()) {

		                    if(item.isEmpty()==true) 
		                        currentRow.setStyle("-fx-background-color:lightcoral");
		                    else
		                        currentRow.setStyle("-fx-background-color:lightgreen");
		                }
		            }
		        };
		    });*/
		System.out.println(":::::::::::::::::::::::::::"+getIdsit()+":::::::::::::::::::::::::::");
		id_treetable.setItems(locations_rech);
    }
	
	public int getIdsit() {
		return idsit;
	}
	public void setIdsit(int idsit) {
		this.idsit = idsit;
	}
	@Override
	public String toString() {
		return "ListEmplacementController [idsit=" + idsit + ", imageHead=" + imageHead + ", id_imagecaptb="
				+ id_imagecaptb + ", id_imagecarton=" + id_imagecarton + ", cartonsImg=" + cartonsImg
				+ ", id_imagerecherche=" + id_imagerecherche + ", HomeImg=" + HomeImg + ", demandesImg=" + demandesImg
				+ ", sitesImg=" + sitesImg + ", personnelImg=" + personnelImg + ", statImg=" + statImg
				+ ", id_codecarton=" + id_codecarton + ", id_codeclient=" + id_codeclient + ", id_dateentree="
				+ id_dateentree + ", id_allee=" + id_allee + ", id_rechercher=" + id_rechercher + ", id_rechercher1="
				+ id_rechercher1 + ", id_treetable=" + id_treetable + ", id_alleecolumn=" + id_alleecolumn
				+ ", id_etagecolumn=" + id_etagecolumn + ", id_colonnecolumn=" + id_colonnecolumn + ", id_videcolumn="
				+ id_videcolumn + ", locations=" + locations + ", locations_rech=" + locations_rech + ", list1=" + list1
				+ ", list2=" + list2 + ", list3=" + list3 + ", list4=" + list4 + ", locations_rech_carton="
				+ locations_rech_carton + ", critere=" + critere + "]";
	}
	public TableView<Location> getId_treetable() {
		return id_treetable;
	}
	public void setId_treetable(TableView<Location> id_treetable) {
		this.id_treetable = id_treetable;
	}
	public TableColumn<Location, Integer> getId_alleecolumn() {
		return id_alleecolumn;
	}
	public void setId_alleecolumn(TableColumn<Location, Integer> id_alleecolumn) {
		this.id_alleecolumn = id_alleecolumn;
	}
	public TableColumn<Location, Integer> getId_etagecolumn() {
		return id_etagecolumn;
	}
	public void setId_etagecolumn(TableColumn<Location, Integer> id_etagecolumn) {
		this.id_etagecolumn = id_etagecolumn;
	}
	public TableColumn<Location, Integer> getId_colonnecolumn() {
		return id_colonnecolumn;
	}
	public void setId_colonnecolumn(TableColumn<Location, Integer> id_colonnecolumn) {
		this.id_colonnecolumn = id_colonnecolumn;
	}
	public TableColumn<Location, Boolean> getId_videcolumn() {
		return id_videcolumn;
	}
	public void setId_videcolumn(TableColumn<Location, Boolean> id_videcolumn) {
		this.id_videcolumn = id_videcolumn;
	}
	
	
	
    
}
