/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import com.jfoenix.controls.JFXButton;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import org.apache.http.client.ClientProtocolException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import jsonrRsources.JsonParserAdmin;
import jsonrRsources.JsonParserAgent;

/**
 * FXML Controller class
 *
 * @author Asusu
 */
public class AjouterAdminController implements Initializable {
    @FXML
    private ImageView imageHead;
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private ImageView HomeImg;
    @FXML
    private ImageView personnelImg;
    @FXML
    private ImageView personnelImg1;
    @FXML
    private TextField matriculeadminlbl;
    @FXML
    private TextField nomadminlbl;
    @FXML
    private TextField adressadminlbl;
    @FXML
    private TextField nteladminlbl;
    @FXML
    private TextField emailadminlbl;
    @FXML
    private TextField mpasseadminlbl;
    @FXML
    private Button ajouterAdmin;
    @FXML
    private JFXButton profil;
    @FXML
    private JFXButton listadminbtn;
    @FXML
    private JFXButton ajouteradminbtn;
    @FXML
    private JFXButton rechercheadminbtn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void homeAction(ActionEvent event) throws IOException {
    	  FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
          Parent root=loader.load();
          Scene scene=new Scene(root);
          Stage  primaryStage= new Stage();
          primaryStage.setScene(scene);
          primaryStage.show();
          Stage stage = (Stage) imageHead.getScene().getWindow();
          stage.close();
    	
    }

    @FXML
    private void logoutAction(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Login.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void addadmin(ActionEvent event) throws IOException, JAXBException {
   	   try {
			
		 JsonParserAdmin.addAdmin(matriculeadminlbl.getText(),nomadminlbl.getText(),mpasseadminlbl.getText(), emailadminlbl.getText(), adressadminlbl.getText(), nteladminlbl.getText());
	   if(true){
		   Alert alert = new Alert(AlertType.INFORMATION);
		   alert.setTitle("Ajouter admin");
		   alert.setHeaderText(null);
		   alert.setContentText("votre noveau admin est enregistr�");
		   alert.showAndWait();
		   matriculeadminlbl.setText("");
		   nomadminlbl.setText("");
		   mpasseadminlbl.setText("");
		   emailadminlbl.setText("");
		   adressadminlbl.setText("");
		   nteladminlbl.setText("");
		   
	   }
	 if(false){
		   Alert alert = new Alert(AlertType.ERROR);
		   
		   alert.setTitle("Error alert");
		   alert.setHeaderText("erreur de saisie manuelle");
		   alert.setContentText("veuillez v�rifier votre saisie");
		    
		 
	   }
   	   } catch (ClientProtocolException e) {
	     	e.printStackTrace();
	  } catch (IOException e) {
		   e.printStackTrace();
	  }
   	 /*
        * Alert alert = new Alert(AlertType.INFORMATION);
alert.setTitle("Information Dialog");
alert.setHeaderText(null);
alert.setContentText("I have a great message for you!");

alert.showAndWait();
        */
   }

    @FXML
    private void showProfil(ActionEvent event) throws IOException {
    	  FXMLLoader loader=new FXMLLoader(getClass().getResource("Profile.fxml"));
          Parent root=loader.load();
          Scene scene=new Scene(root);
          Stage  primaryStage= new Stage();
          primaryStage.setScene(scene);
          primaryStage.show();
          Stage stage = (Stage) imageHead.getScene().getWindow();
          stage.close();
      
    }

    @FXML
    private void getlistadmin(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("getlistadmin.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void ajouteradmin(ActionEvent event) throws IOException {
    	 FXMLLoader loader=new FXMLLoader(getClass().getResource("AjouterAdmin.fxml"));
         Parent root=loader.load();
         Scene scene=new Scene(root);
         Stage  primaryStage= new Stage();
         primaryStage.setScene(scene);
         primaryStage.show();
         Stage stage = (Stage) imageHead.getScene().getWindow();
         stage.close();
    }

    @FXML
    private void rechercheadmin(ActionEvent event) {
    }
    
}
