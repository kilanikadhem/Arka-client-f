/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import com.jfoenix.controls.JFXButton;

import arka.domain.Admin;
import arka.domain.Agent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javassist.Loader;
import jsonrRsources.JsonParserAdmin;
import jsonrRsources.JsonParserAgent;
import jsonrRsources.JsonParserDemand;
import java.lang.Integer.*;
/**
 * FXML Controller class
 *
 * @author Asusu
 */
public class ProfileController implements Initializable {
    @FXML
    private ImageView imageHead;
    @FXML
    private ImageView LogoutImg;
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private JFXButton listagentbtn;
    @FXML
    private ImageView HomeImg;
    @FXML
    private ImageView personnelImg;
    @FXML
    private JFXButton ajouteragentbtn;
    @FXML
    private ImageView personnelImg1;
    @FXML
    private JFXButton rechercheagentbtn;
    @FXML
    private JFXButton profil;
    @FXML
    private Text idadminprofil;
    @FXML
    private Text nomprofil;
    @FXML
    private Text numtelprofile;
    @FXML
    private Text adressprofil;
    @FXML
    private Text emailprofil;

    /**
     * Initializes the controller class.
     */
    int id ;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	  FXMLLoader loader1 = new FXMLLoader(getClass().getResource("Login.fxml"));
          Parent root1;
         
		try { root1 = (Parent) loader1.load();
        LoginController l= loader1.getController();          
	    int id= l.id;
	   System.out.println(l.id);
			 Admin admin = JsonParserAdmin.getAdminbyid("http://localhost:18080/Arka-web/api/admin/"+id);
			 System.out.println(admin.getNom());
			 nomprofil.setText(admin.getNom());
			 adressprofil.setText(admin.getPrenon());
			 emailprofil.setText(admin.getEmail());
			String i=Integer.toString(admin.getIdAdmin());
			  idadminprofil.setText( i);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      	
          
    	//Agent agent = JsonParserAgent.getAgentbyid("http://localhost:18080/Arka-web/api/user/"+LoginController.id);
    	
    }    

    @FXML
    private void homeAction(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void logoutAction(ActionEvent event)throws IOException  {
    	 FXMLLoader loader=new FXMLLoader(getClass().getResource("Login.fxml"));
         Parent root=loader.load();
         Scene scene=new Scene(root);
         Stage  primaryStage= new Stage();
         primaryStage.setScene(scene);
         primaryStage.show();
          Stage stage = (Stage) imageHead.getScene().getWindow();
      stage.close();
    }

    @FXML
    private void getlistagent(ActionEvent event)throws IOException  {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListAgent.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void ajouteragent(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("AjouteAgent.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void rechercheagent(ActionEvent event)throws IOException  {
    }

    @FXML
    private void showProfil(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Profile.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }


	public void setIdadminprofil(String txt) {
		idadminprofil.setText(txt);
	}

	

	public void setNomprofil(String txt) {
		nomprofil.setText(txt);
	}

	
	public void setNumtelprofile(String txt) {
		numtelprofile.setText(txt);
	}

	
	public void setAdressprofil(String txt) {
		adressprofil.setText(txt);
	}

	

	public void setEmailprofil(String txt) {
		emailprofil.setText(txt);
	}

  
}
