
package fxml;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.http.client.ClientProtocolException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;

import arka.domain.Client;
import arka.domain.Demand;
import arka.domain.DemandState;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import jsonrRsources.JsonParserDemand;


/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class ListDemandsController implements Initializable {

    /**
     * Initializes the controller class.
     */
	  @FXML
	  private Text clientTxt;
	  
	  @FXML
	  private Text idclientTxt;
	  
	  
	  @FXML
	    private JFXListView<Demand> listDemands;
	  private ObservableList<Demand> data=FXCollections.observableArrayList();
	
	  String nomClient;
	int idCLient;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	System.out.println("*****+"+ClientInformationsController.c);
    	 data.clear();
    	 HBox hbox = new HBox();
    	  
    	List<Demand> demands = new ArrayList<Demand>();
		try {
			System.out.println("http://localhost:18080/Arka-web/api/demandes/DemandsClient/"+ClientInformationsController.c);
			demands = JsonParserDemand.getDemandListByCLient("http://localhost:18080/Arka-web/api/demandes/DemandsClient",ClientInformationsController.c);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		  Collections.reverse(demands);
	        for (Demand demand : demands ) {
	         	    System.out.println(demand.getIdDemand());
	         	   
	         		data.add(demand);		
	 			 
	         
	         }
	        
	        listDemands.setCellFactory(new Callback<ListView<Demand>, ListCell<Demand>>(){
	            @Override
	            public ListCell<Demand> call(ListView<Demand> args0) {
	                Image img= new Image("assets/file.png");
	                ImageView imageview=new ImageView(img);
	                Label label=new Label("");
	                JFXButton buttClient=new JFXButton();
	                //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
	               
	                JFXButton buttProfil=new JFXButton();
	               // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
	               buttProfil.setMinHeight(30);
	                buttProfil.setMinWidth(32);
	                JFXButton buttAffecter=new JFXButton();
	                
	             
	                buttAffecter.setAlignment(Pos.CENTER_RIGHT);
	                HBox hbox = new HBox();
	                hbox.getChildren().addAll(imageview, label,buttProfil,buttClient,buttAffecter);
	                
	                
	                
	                ListCell<Demand> cell;
	                int i = 0 ;
	                cell = new ListCell<Demand>(){
	                    
	                    @Override
	                    protected void updateItem(Demand r ,boolean b){
	                 	  
	                        super.updateItem(r,b);
	                        
	                          if(r != null){
	                        	  buttClient.setOnAction((event) -> {
	                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("ClientInformations.fxml"));
	                        		  
	                                  Parent root;
									try {
										root = (Parent) loader.load();
										ClientInformationsController ctrl = loader.getController();
		                                  ctrl.setAdresse(r.getClient().getAdress());
		                                  ctrl.setnomText(r.getClient().getNom());
		                                  ctrl.setmailtext(r.getClient().getEmail());
		                                  ctrl.setTelTxt(r.getClient().getNumTel());
		                                  ctrl.setRespText(r.getClient().getNomResp());
		                                  ctrl.setCleint(r.getClient().getIdClient());
		                                  Scene newScene = new Scene(root);
		                                  Stage newStage = new Stage();
		                                  newStage.setTitle("Informations");
		                                  newStage.setScene(newScene);
		                                  newStage.show();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
	                                  
	                        		  
	                        	  });
	                         	 if(r.getDemandState()==DemandState.nonTraitee){
	                         		
	                         		 hbox.setStyle("-fx-background-color:#ff9999;");
	                       	   }else if(r.getDemandState()==DemandState.affectee){
	                       		buttAffecter.setDisable(true);
	                       		 hbox.setStyle("-fx-background-color:#fda36f;");
	                       	   }else{
	                       		hbox.setStyle("-fx-background-color:#bef67a;");
	                       		   
	                       	   }
	                            label.setText("Client:" +r.getClient().getNom()+" Nom :"+r.getClient().getNomResp() + " Type: "+r.getDemandType()+ " Date :" + r.getDate() );
	                            setGraphic(hbox);}
	                            
	                          
	                          
	                       
	                              
	                            
	                           
	                            
	                             
	                            
	                            
	                        
	                     
	                        }
	                        
	                      
	                 
	                };
	                
	                
	             return cell;    
	            }
	             
	           
	        
	                 });
	        listDemands.setItems(data);
	         
	        
    }    
    
    public void setclientTxt(String txt){
    	
    	clientTxt.setText(txt);
    	
    	
    }
    
    public void setClient(int c){
    	
    	idclientTxt.setText(c+"");
    }
    
    @FXML
    private void cancelAction(ActionEvent event) {
    	
           Stage stage = (Stage) clientTxt.getScene().getWindow();
       // do what you have to do
       stage.close();
    }
    
 
    
}
