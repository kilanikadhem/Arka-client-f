package fxml;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.boot.registry.selector.StrategyRegistration;
import org.hibernate.service.spi.Startable;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.*;
import javafx.concurrent.*;
import javafx.fxml.FXMLLoader;
import javafx.geometry.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.*;
import javafx.util.Duration;

public class TestInterface extends Application {
	
	 @Override
	    public void start(Stage primaryStage) throws IOException {
		 FXMLLoader loader=new FXMLLoader(getClass().getResource("AgentDemandPersoFXML.fxml"));
	    	//FXMLLoader loader=new FXMLLoader(getClass().getResource("formationHr.fxml"));
	    	
	       Parent root=loader.load();
	       Scene scene=new Scene(root);
	       primaryStage.setScene(scene);
	       primaryStage.show();
	    }

	    /**
	     * @param args the command line arguments
	     */
	    public static void main(String[] args) {
	        launch(args);
	    }
	      


	
}

	

