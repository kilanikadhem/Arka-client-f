/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fxml;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;

import arka.domain.Carton;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.http.client.ClientProtocolException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import jsonrRsources.JsonParserDemand;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class CartonListFXMLController implements Initializable {

    @FXML
    private JFXListView<Carton> ListCartons;
    private ObservableList<Carton> data=FXCollections.observableArrayList();
    @FXML
    private Text ClientTxt;
    @FXML
    private Text DateTxt;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    } 
    
    public void setCLientName(String name){
    	ClientTxt.setText(name);
    }
    
    public void setDate(String date){
    	DateTxt.setText(date);
    }
    
    public void fillListCartons(List<Carton> cartons){
    	data.clear();
   	 HBox hbox = new HBox();
   	  
   
		  Collections.reverse(cartons);
	        for (Carton demand : cartons ) {
	         	    
	         	   
	         		data.add(demand);		
	 			 
	         
	         }
	        
	        ListCartons.setCellFactory(new Callback<ListView<Carton>, ListCell<Carton>>(){
	            @Override
	            public ListCell<Carton> call(ListView<Carton> args0) {
	                Image img= new Image("assets/file.png");
	                ImageView imageview=new ImageView(img);
	                Label label=new Label("");
	                JFXButton buttClient=new JFXButton();
	                //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
	               
	                JFXButton buttProfil=new JFXButton();
	               // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
	               buttProfil.setMinHeight(30);
	                buttProfil.setMinWidth(32);
	                JFXButton buttAffecter=new JFXButton();
	                
	             
	                buttAffecter.setAlignment(Pos.CENTER_RIGHT);
	                HBox hbox = new HBox();
	                hbox.getChildren().addAll(imageview, label,buttProfil,buttClient,buttAffecter);
	                
	                
	                
	                ListCell<Carton> cell;
	                int i = 0 ;
	                cell = new ListCell<Carton>(){
	                    
	                    @Override
	                    protected void updateItem(Carton r ,boolean b){
	                 	  
	                        super.updateItem(r,b);
	                        
	                          if(r != null){
	                        	  buttClient.setOnAction((event) -> {
	                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("ClientInformations.fxml"));
	                        		  
	                                  Parent root;
									try {
										root = (Parent) loader.load();
										ClientInformationsController ctrl = loader.getController();
		                                  ctrl.setAdresse(r.getClient().getAdress());
		                                  ctrl.setnomText(r.getClient().getNom());
		                                  ctrl.setmailtext(r.getClient().getEmail());
		                                  ctrl.setTelTxt(r.getClient().getNumTel());
		                                  ctrl.setRespText(r.getClient().getNomResp());
		                                  ctrl.setCleint(r.getClient().getIdClient());
		                                  Scene newScene = new Scene(root);
		                                  Stage newStage = new Stage();
		                                  newStage.setTitle("Informations");
		                                  newStage.setScene(newScene);
		                                  newStage.show();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
	                                  
	                        		  
	                        	  });
	                         	
	                            label.setText("Carton : "+r.getIdCarton() +" Client:" +r.getClient().getNom()+" Nom :"+r.getClient().getNomResp() + " Date d'entrée : "+r.getArrivalDate()+ " Duree:" + r.getDuration());
	                            setGraphic(hbox);}
	                            
	                          
	                          
	                       
	                              
	                            
	                           
	                            
	                             
	                            
	                            
	                        
	                     
	                        }
	                        
	                      
	                 
	                };
	                
	                
	             return cell;    
	            }
	             
	           
	        
	                 });
	        ListCartons.setItems(data);
	         
	        
    	
    }
}
