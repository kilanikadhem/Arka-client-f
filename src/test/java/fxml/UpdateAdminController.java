/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import org.apache.http.client.ClientProtocolException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import jsonrRsources.JsonParserAdmin;
import jsonrRsources.JsonParserAgent;

import java.io.IOException;
import java.lang.String.*;
/**
 * FXML Controller class
 *
 * @author Asusu
 */
public class UpdateAdminController implements Initializable {
    @FXML
    private Button saveadminbtn;
    @FXML
    private Button cancelbtn;
    @FXML
    private JFXTextField matriculetxt;
    @FXML
    private JFXTextField idtxt;
    @FXML
    private JFXTextField nomtxt;
    @FXML
    private JFXTextField prenomtxt;
    @FXML
    private JFXTextField emailtxt;
    @FXML
    private JFXTextField numteltxt;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	idtxt.setVisible(false);
        idtxt.setDisable(true);
    }    

    public void setidtxt(int txt){
    	idtxt.setText(String.valueOf(txt) );
    }
    public void setmatriculetxt(String txt){
    	matriculetxt.setText(txt);
    }
    public void setnomtxt(String txt){
    	nomtxt.setText(txt);
    }
    public void setprenontxt(String txt){
    	prenomtxt.setText(txt);
    }
    public void setemailtxt(String txt){
    	emailtxt.setText(txt);
    }
    public void setnumteltxt(String txt){
    	numteltxt.setText(txt);
    }
    
    @FXML
    private void updateadmin(ActionEvent event) throws NumberFormatException, JAXBException {
    	 try {
			 JsonParserAdmin.upadateAdmin( Integer.parseInt(idtxt.getText()),matriculetxt.getText(), nomtxt.getText(), emailtxt.getText(),prenomtxt.getText(),numteltxt.getText());
		
			
			  Stage stage = (Stage) cancelbtn.getScene().getWindow();
			    stage.close();
         } catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    private void cancelupdate(ActionEvent event) {
    	Stage stage = (Stage) cancelbtn.getScene().getWindow();
	    stage.close();
    }
    
}
