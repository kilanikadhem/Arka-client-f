/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import com.jfoenix.controls.JFXButton;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import org.apache.http.client.ClientProtocolException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import jsonrRsources.JsonParserAdmin;
import jsonrRsources.JsonParserClient;

/**
 * FXML Controller class
 *
 * @author Asusu
 */
public class AjouterClientController implements Initializable {
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private ImageView HomeImg;
    @FXML
    private TextField nomclientlbl;
    @FXML
    private TextField nomresplbl;
    @FXML
    private TextField  numtllbl;
    @FXML
    private TextField mpasseclientbl;
    @FXML
    private JFXButton profil;
    @FXML
    private TextField codeclientlbl;
    @FXML
    private ImageView imageHead;
    @FXML
    private JFXButton listclientbtn;
    @FXML
    private JFXButton ajouterclientbtn;
    @FXML
    private ImageView personnelImg1;
    @FXML
    private ImageView personnelImg;
    @FXML
    private TextField emailclientlbl;
    @FXML
    private Button ajouterClient;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void homeAction(ActionEvent event) throws IOException{
  	  FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
      Parent root=loader.load();
      Scene scene=new Scene(root);
      Stage  primaryStage= new Stage();
      primaryStage.setScene(scene);
      primaryStage.show();
      Stage stage = (Stage) imageHead.getScene().getWindow();
      stage.close();
    }

    @FXML
    private void logoutAction(ActionEvent event) throws IOException{
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Login.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void showProfil(ActionEvent event)throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Profile.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void getlistclient(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("getlistclient.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void ajouterclient(ActionEvent event) throws IOException{
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ajouterclient.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void addclient(ActionEvent event) throws IOException, JAXBException{
    	
    	 try {
 			
    		 JsonParserClient.addClient(codeclientlbl.getText(),nomclientlbl.getText(),mpasseclientbl.getText(),emailclientlbl.getText(), nomresplbl.getText(),numtllbl.getText());
    	   if(true){
    		   Alert alert = new Alert(AlertType.INFORMATION);
    		   alert.setTitle("Ajouter admin");
    		   alert.setHeaderText(null);
    		   alert.setContentText("votre noveau admin est enregistré");
    		   alert.showAndWait();
    		   codeclientlbl.setText("");
    		   nomclientlbl.setText("");
    		   nomresplbl.setText("");
    		   emailclientlbl.setText("");
    		   mpasseclientbl.setText("");
    		   numtllbl.setText("");
    		   
    	   }
    	
       	   } catch (ClientProtocolException e) {
    	     	e.printStackTrace();
    	  } catch (IOException e) {
    		   e.printStackTrace();
    	  }
    }
    
}
