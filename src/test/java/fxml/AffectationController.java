/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fxml;

import com.jfoenix.controls.JFXComboBox;

import arka.domain.Agent;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.http.client.ClientProtocolException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import jsonrRsources.JsonParserAgent;
import jsonrRsources.JsonParserDemand;
import javafx.scene.control.*;
import javafx.scene.control.Alert.*;
import java.util.Optional;
/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class AffectationController implements Initializable {

    @FXML
    private Text demandTxt;
    @FXML
    private Text dateTxt;
    @FXML
    private JFXComboBox<Agent> comboAgetns;
    @FXML
    private Text NomTxt;
    @FXML
    private Text nbreAttente;
    @FXML
    private Text idDem;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	List<Agent> agents = new ArrayList<Agent>();
		try {
			agents = JsonParserAgent.getAgentList("http://localhost:18080/Arka-web/api/user");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		comboAgetns.getItems().addAll(agents);
    }    

    @FXML
    private void ChangeAction(ActionEvent event) {
    	NomTxt.setText(comboAgetns.getValue().getNom()  );
    	nbreAttente.setText(comboAgetns.getValue().getMatricule());
    }

    @FXML
    private void AffecterAction(ActionEvent event) throws ClientProtocolException, IOException {
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Confirmation de L'affectation ");
    	alert.setHeaderText(" La demande de "+dateTxt.getText()+" sera aeffectée a "  +comboAgetns.getValue().getNom()+" Matricle :"+comboAgetns.getValue().getMatricule());
    	alert.setContentText("Confirmation de cette Action?");

    	Optional<ButtonType> result = alert.showAndWait();
    	if (result.get() == ButtonType.OK){
    		JsonParserDemand.affecter("http://localhost:18080/Arka-web/api/demandes/affecter",Integer.parseInt(idDem.getText()),comboAgetns.getValue().getMatricule());
    	} else {
    	   alert.close();
    	}
    	
    }
    
    public void setText(String demandClient, String date,String idDemande){
    	demandTxt.setText(demandClient);
    	dateTxt.setText(date+"");
    	idDem.setText(idDemande);
    }
    
}
