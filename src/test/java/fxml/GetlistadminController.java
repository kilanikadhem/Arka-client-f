/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;
import javafx.event.ActionEvent;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;

import arka.domain.Admin;
import arka.domain.Agent;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.http.client.ClientProtocolException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import jsonrRsources.JsonParserAdmin;
import jsonrRsources.JsonParserAgent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import java.util.Optional;
/**
 * FXML Controller class
 *
 * @author Asusu
 */
public class GetlistadminController implements Initializable {
    @FXML
    private JFXListView<Admin> listadmin;
    @FXML
    private ImageView imageHead;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private ImageView LogoutImg;
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private ImageView HomeImg;
    @FXML
    private ImageView personnelImg;
    @FXML
    private ImageView personnelImg1;
    @FXML
    private JFXButton profil;
    @FXML
    private JFXButton listadminbtn;
    @FXML
    private JFXButton ajouteradminbtn;
    @FXML
    private JFXButton rechercheadminbtn;

    /**
     * Initializes the controller class.
     */
    private ObservableList<Admin> data=FXCollections.observableArrayList();
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	data.clear();
   	 HBox hbox = new HBox();
   	List<Admin>admins = new ArrayList<Admin>();
		try {
			System.out.println("http://localhost:18080/Arka-web/api/admin");
			admins = JsonParserAdmin.getAdminList("http://localhost:18080/Arka-web/api/admin");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (Admin admin : admins) {
			System.out.println (admin.getNom());	}

		Collections.reverse(admins);
       for (Admin admin : admins) {
               System.out.print(admin.getNom());
              
               data.add(admin);       
            
        
        }
       listadmin.setCellFactory(new Callback<ListView<Admin>, ListCell<Admin>>(){
           @Override
           public ListCell<Admin> call(ListView<Admin> args0) {
           	Image img= new Image("assets/file.png");
               ImageView imageview=new ImageView(img);
        
               Label label=new Label("");
               JFXButton buttdelete=new JFXButton();
               buttdelete.setStyle("-fx-background-image: url('/assets/delete.png');");
               buttdelete.setMinHeight(30);
               buttdelete.setMinWidth(30);
           	Image img4= new Image("assets/file.png");
            ImageView imageview4=new ImageView(img);
      
              // buttdelete.setAlignment();
               JFXButton buttProfil=new JFXButton();
              buttProfil.setStyle("-fx-background-image: url('/assets/send.png');");
               buttProfil.setMinHeight(30);
               buttProfil.setMinWidth(30);
              
               
              
               buttProfil.setAlignment(Pos.CENTER_RIGHT);
               HBox hbox = new HBox();
               hbox.getChildren().addAll(imageview, label,buttdelete,buttProfil);
               
          
               ListCell<Admin> cell;
               int i = 0 ;
               cell = new ListCell<Admin>(){
                  
               	@Override
                   protected void updateItem(Admin r ,boolean b){
                     
                       super.updateItem(r,b);
                       
                         if(r != null){
                       	  buttdelete.setOnAction((event) -> {
                     		 
                       		FXMLLoader loader1 = new FXMLLoader(getClass().getResource("Login.fxml"));
                            Parent root1;
                     			try {root1 = (Parent) loader1.load();
                     				LoginController l= loader1.getController();          
                     			    int id= l.id;
                     				
                     				if(r.getIdAdmin()!=1 ){
                     					if(r.getIdAdmin()!=id){
										Alert alert = new Alert(AlertType.CONFIRMATION);
										alert.setTitle("Confirmation Dialog");
										alert.setHeaderText("Look, a Confirmation Dialog");
										alert.setContentText("Are you ok with this?");

										Optional<ButtonType> result = alert.showAndWait();
										if (result.get() == ButtonType.OK){
											JsonParserAdmin.DeleteAdmin(r.getIdAdmin());
											Stage stage = (Stage) imageHead.getScene().getWindow();
										     stage.close();
										     FXMLLoader loader=new FXMLLoader(getClass().getResource("Getlistadmin.fxml"));
								             Parent root=loader.load();
								        Scene scene=new Scene(root);
								        Stage  primaryStage= new Stage();
								        primaryStage.setScene(scene);
								        primaryStage.show();
										}}}
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
                     	    	
                     		  
                     		 
                     	  });
                       		 
                       		
                       	 
                             buttProfil.setOnAction((event) -> {
                                FXMLLoader loader = new FXMLLoader(getClass().getResource("updateAdmin.fxml"));
                                 
                                 Parent root;
                                 
                               try {
                               	
                                 root = (Parent) loader.load();
                                 UpdateAdminController update= loader.getController();
                             	//System.out.println(r.getNom());            
                             	update.setnomtxt(r.getNom());
                             	update.setemailtxt(r.getEmail());
                             	update.setprenontxt(r.getPrenon());
                                  update.setmatriculetxt(r.getMatricule());
                                  update.setidtxt(r.getIdAdmin());
                                  update.setnumteltxt(r.getNumTel());
	                                  Scene newScene = new Scene(root);
                                   Stage newStage = new Stage();
                                   newStage.setTitle("Informations");
                                   newStage.setScene(newScene);
                                   newStage.show();
                                   
                               } catch (Exception e) {
                                   // TODO Auto-generated catch block
                                   e.printStackTrace();
                               }
                                 
                               
                             });
                           
                           label.setText("Admin" +r.getIdAdmin()+" Matricule: " +r.getMatricule()+" Nom :"+r.getNom() + " Pr�nom: "+r.getPrenon()+ " email :"+r.getEmail());
                           setGraphic(hbox);}
                           
                         
                         
                      
                             
                           
                          
                           
                            
                           
                           
                       
                    
                       }
                       
                     
                
               };
               
               
            return cell;    
           }
            
          
       
                });
       listadmin.setItems(data);
        // TODO
    }    

    @FXML
    private void logoutAction(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Login.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
         Stage stage = (Stage) imageHead.getScene().getWindow();
     stage.close();
    }

    @FXML
    private void homeAction(ActionEvent event) throws IOException {
    	 FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
         Parent root=loader.load();
         Scene scene=new Scene(root);
         Stage  primaryStage= new Stage();
         primaryStage.setScene(scene);
         primaryStage.show();
         Stage stage = (Stage) imageHead.getScene().getWindow();
         stage.close();
    }

    @FXML
    private void showProfil(ActionEvent event) throws IOException {
    	  FXMLLoader loader=new FXMLLoader(getClass().getResource("Profile.fxml"));
          Parent root=loader.load();
          Scene scene=new Scene(root);
          Stage  primaryStage= new Stage();
          primaryStage.setScene(scene);
          primaryStage.show();
          Stage stage = (Stage) imageHead.getScene().getWindow();
          stage.close();
    }

    @FXML
    private void getlistadmin(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("getlistadmin.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void ajouteradmin(ActionEvent event) throws IOException {
    	 FXMLLoader loader=new FXMLLoader(getClass().getResource("AjouterAdmin.fxml"));
         Parent root=loader.load();
         Scene scene=new Scene(root);
         Stage  primaryStage= new Stage();
         primaryStage.setScene(scene);
         primaryStage.show();
         Stage stage = (Stage) imageHead.getScene().getWindow();
         stage.close();
    }

    @FXML
    private void rechercheadmin(ActionEvent event) {
    }
    
}
