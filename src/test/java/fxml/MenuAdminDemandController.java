/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fxml;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import javafx.geometry.Pos;
import arka.domain.DemandState;
import arka.domain.DemandType;
import arka.domain.*;
import java.io.IOException;
import java.net.URL;
import java.nio.file.attribute.PosixFilePermission;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.util.Callback;
import javax.xml.crypto.Data;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import org.apache.http.client.ClientProtocolException;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import jsonrRsources.JsonParserDemand;
import javafx.scene.layout.HBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class MenuAdminDemandController implements Initializable {

    @FXML
    private ImageView imageHead;
    @FXML
    private ImageView LogoutImg;
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton CartonBtn;
    @FXML
    private JFXButton DemandesBtn;
    @FXML
    private JFXButton SitesBtn;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private JFXButton PersonnelBtn;
    @FXML
    private JFXButton PersonnelBtn1;
    @FXML
    private ImageView HomeImg;
    @FXML
    private ImageView cartonsImg;
    @FXML
    private ImageView demandesImg;
    @FXML
    private ImageView sitesImg;
    @FXML
    private ImageView personnelImg;
    @FXML
    private ImageView statImg;
    @FXML
    private JFXDatePicker datepicktxt;
    @FXML
    private JFXComboBox<DemandType> typeCombo;
    @FXML
    private JFXComboBox<DemandState> etatCombo;
    @FXML
    private JFXTextField ClientTxt;
    @FXML
    private ImageView imgSearch;
    @FXML
    private JFXButton search;
    @FXML
    private JFXListView<Demand> listViewDemands;
    
    private ObservableList<Demand> data=FXCollections.observableArrayList();
    
    @FXML
    private ImageView actualiserImg;
    @FXML
    private JFXButton refrechBtn;
    @FXML
    private JFXButton agentsBtn;

    /**
     * Initializes the controller class.
     */
   
    public void initialize(URL url, ResourceBundle rb) {
    	Image img = new Image("assets/hrhead.jpg");
   	 imageHead.setImage(img);
        img = new Image("assets/logout.png");
        LogoutImg.setImage(img);
        img = new Image("assets/home.png");
        HomeImg.setImage(img);
        img = new Image("assets/archive.png");
        cartonsImg.setImage(img);
        img = new Image("assets/send.png");
        demandesImg.setImage(img);
        img = new Image("assets/placeholder.png");
        sitesImg.setImage(img);
        img = new Image("assets/users.png");
        personnelImg.setImage(img);
        img = new Image("assets/projectmenu.png");
        statImg.setImage(img);
        img = new Image("assets/search.png");
        imgSearch.setImage(img);
        img = new Image("assets/refresh.png");
        actualiserImg.setImage(img);
      
        typeCombo.getItems().addAll(DemandType.Add,DemandType.Retrait,DemandType.Expedition,DemandType.Divers);
        etatCombo.getItems().addAll(DemandState.nonTraitee,DemandState.affectee,DemandState.affectee);
        HBox hbox = new HBox();
        data.clear();
        
     	
 		
 		List<Demand> demands = new ArrayList<Demand>();
		try {
			demands = JsonParserDemand.getDemandList("http://localhost:18080/Arka-web/api/demandes");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
        Collections.reverse(demands);
        for (Demand demand : demands ) {
         	    System.out.print(demand.getIdDemand());
         	 
  
         		data.add(demand);		
         
         }
        listViewDemands.setCellFactory(new Callback<ListView<Demand>, ListCell<Demand>>(){
            @Override
            public ListCell<Demand> call(ListView<Demand> args0) {
                Image img= new Image("assets/file.png");
                ImageView imageview=new ImageView(img);
                Label label=new Label("");
                JFXButton buttClient=new JFXButton();
                //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
                buttClient.setStyle("-fx-background-image: url('/assets/info.png');-fx-font-size: 1em; ");
                buttClient.setMinHeight(30);
                buttClient.setMinWidth(30);
                JFXButton buttProfil=new JFXButton();
               // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
               buttProfil.setMinHeight(30);
                buttProfil.setMinWidth(32);
                JFXButton buttAffecter=new JFXButton();
                
                buttAffecter.setMinHeight(30);
                buttAffecter.setMinWidth(30);
                buttAffecter.setStyle("-fx-background-image: url('/assets/send.png');");
                buttAffecter.setAlignment(Pos.CENTER_RIGHT);
                JFXButton buttCartons=new JFXButton();
                 buttCartons.setStyle("-fx-background-image: url('/assets/archive.png');");
                buttCartons.setMinHeight(30);
                buttCartons.setMinWidth(32);
                HBox hbox = new HBox();
                hbox.getChildren().addAll(imageview, label,buttProfil,buttClient,buttAffecter,buttCartons);
                
                
                
                ListCell<Demand> cell;
                int i = 0 ;
                cell = new ListCell<Demand>(){
                    
                    @Override
                    protected void updateItem(Demand r ,boolean b){
                 	  
                        super.updateItem(r,b);
                        
                          if(r != null){
                        	  buttCartons.setOnAction((event)->{
                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("cartonListFXML.fxml"));
                        		  Parent root;
                        		  try {
                        			  
									root = (Parent) loader.load();
									CartonListFXMLController clfc= loader.getController();
									clfc.setCLientName(r.getClient().getNom());
									clfc.setDate(r.getDate()+"");
									System.out.println("sizee = "+r.getCartons().size());
									clfc.fillListCartons(r.getCartons());
									  Scene newScene = new Scene(root);
	                                  Stage newStage = new Stage();
	                                  newStage.setTitle("Informations");
	                                  newStage.setScene(newScene);
	                                  newStage.show();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
									  
                        		  
                        	  });
                        	  buttAffecter.setOnAction((event) -> {
                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("Affectation.fxml"));
                        		  Parent root;
                        		  try {
                        			  
									root = (Parent) loader.load();
									AffectationController afctrl=  loader.getController();
									afctrl.setText(r.getClient().getNom(), r.getDate()+"",r.getIdDemand()+"");
									  Scene newScene = new Scene(root);
	                                  Stage newStage = new Stage();
	                                  newStage.setTitle("Informations");
	                                  newStage.setScene(newScene);
	                                  newStage.show();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
									
	                                 
                        	  });
                        	  buttClient.setOnAction((event) -> {
                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("ClientInformations.fxml"));
                        		  
                                  Parent root;
								try {
									root = (Parent) loader.load();
									ClientInformationsController ctrl = loader.getController();
	                                  ctrl.setAdresse(r.getClient().getAdress());
	                                  ctrl.setnomText(r.getClient().getNom());
	                                  ctrl.setmailtext(r.getClient().getEmail());
	                                  ctrl.setTelTxt(r.getClient().getNumTel());
	                                  ctrl.setRespText(r.getClient().getNomResp());
	                                  ctrl.setCleint(r.getClient().getIdClient());
	                                  Scene newScene = new Scene(root);
	                                  Stage newStage = new Stage();
	                                  newStage.setTitle("Informations");
	                                  newStage.setScene(newScene);
	                                  newStage.show();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
                                  
                        		  
                        	  });
                         	 if(r.getDemandState()==DemandState.nonTraitee){
                         		
                         		 hbox.setStyle("-fx-background-color:#ff9999;");
                       	   }else if(r.getDemandState()==DemandState.affectee){
                       		 buttAffecter.setDisable(true);
                       		 hbox.setStyle("-fx-background-color:#fda36f;");
                       	   }else{
                       		hbox.setStyle("-fx-background-color:#bef67a;");
                       		   
                       	   }
                            label.setText("Client:" +r.getClient().getNom()+" Nom :"+r.getClient().getNomResp() + " Type: "+r.getDemandType()+ " Date :"+r.getDate());
                            setGraphic(hbox);}

                        
                     
                        }
                        
                      
                 
                };
                
                
             return cell;    
            }
             
           
        
                 });
        listViewDemands.setItems(data);
         
       
        
 	}
        

    @FXML
    private void homeAction(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void cartonsBtn(ActionEvent event) {
    }

    @FXML
    private void demandesAction(ActionEvent event) {
    }

    @FXML
    private void sitesActions(ActionEvent event) {
    }

    @FXML
    private void logoutAction(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("Login.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
    // do what you have to do
    stage.close();
    }

    @FXML
    private void personnelAction(ActionEvent event) {
    }

    @FXML
    private void searchAction(ActionEvent event) throws ClientProtocolException, IOException {
    	System.out.println(typeCombo.getValue());
    	System.out.println(etatCombo.getValue());
    	System.out.println(datepicktxt.getValue());
    	System.out.println("**"+ClientTxt.getText()+"**");
    	System.out.println(ClientTxt.getText().equals(""));
    	System.out.println("test Combo");
    	if((ClientTxt.getText().equals("")==false)&&(typeCombo.getValue()==null)&&(etatCombo.getValue()==null)&&(datepicktxt.getValue()==null)){
    		
    		 FilterByName(ClientTxt.getText());
    	}
    	if((ClientTxt.getText().equals("")==false)&&(typeCombo.getValue()==null)&&(etatCombo.getValue()==null)&&(datepicktxt.getValue()!=null)){
    		filterByClientDate(ClientTxt.getText(),datepicktxt.getValue());
    	}
    	if((ClientTxt.getText().equals("")==false)&&(typeCombo.getValue()!=null)&&(etatCombo.getValue()==null)&&(datepicktxt.getValue()!=null)){
    		filterByClientTypeDate(ClientTxt.getText(),typeCombo.getValue(),datepicktxt.getValue());
    	}
    	if((ClientTxt.getText().equals("")==false)&&(typeCombo.getValue()!=null)&&(etatCombo.getValue()!=null)&&(datepicktxt.getValue()!=null)){
    		filterByClientTypeDateEtat(ClientTxt.getText(),typeCombo.getValue(),datepicktxt.getValue(),etatCombo.getValue());
    	}
    	if((ClientTxt.getText().equals("")==false)&&(typeCombo.getValue()!=null)&&(etatCombo.getValue()!=null)&&(datepicktxt.getValue()==null)){
    		filterByClientTypeEtat(ClientTxt.getText(),typeCombo.getValue(),etatCombo.getValue());
    		
    		
    	}
    	if((ClientTxt.getText().equals("")==false)&&(typeCombo.getValue()!=null)&&(etatCombo.getValue()==null)&&(datepicktxt.getValue()==null)){
    		filterByClientType(ClientTxt.getText(),typeCombo.getValue());
    	}
    	if((ClientTxt.getText().equals("")==false)&&(typeCombo.getValue()==null)&&(etatCombo.getValue()!=null)&&(datepicktxt.getValue()==null)){
    		
    		filterByClientEtat(ClientTxt.getText(),etatCombo.getValue());
    	}
    	if((ClientTxt.getText().equals("")==false)&&(typeCombo.getValue()==null)&&(etatCombo.getValue()!=null)&&(datepicktxt.getValue()!=null)){
    		filterByClientEtatDate(ClientTxt.getText(),etatCombo.getValue(),datepicktxt.getValue());
    	}
    	if((ClientTxt.getText().equals("")==true)&&(typeCombo.getValue()!=null)&&(etatCombo.getValue()==null)&&(datepicktxt.getValue()!=null)){
    		filterByTypeDate(typeCombo.getValue(),datepicktxt.getValue());
    	}
    	if((ClientTxt.getText().equals("")==true)&&(typeCombo.getValue()==null)&&(etatCombo.getValue()!=null)&&(datepicktxt.getValue()!=null)){
    		filterByEtatDate(etatCombo.getValue(),datepicktxt.getValue());
    	}
    	if((ClientTxt.getText().equals("")==true)&&(typeCombo.getValue()!=null)&&(etatCombo.getValue()!=null)&&(datepicktxt.getValue()!=null)){
    		filterByTypeEtatDate(typeCombo.getValue(),etatCombo.getValue(),datepicktxt.getValue());
    	}
    	if((ClientTxt.getText().equals("")==true)&&(typeCombo.getValue()!=null)&&(etatCombo.getValue()!=null)&&(datepicktxt.getValue()==null)){
    		filterByTypeState(typeCombo.getValue(),etatCombo.getValue());
    	}
    	if((ClientTxt.getText().equals("")==true)&&(typeCombo.getValue()==null)&&(etatCombo.getValue()!=null)&&(datepicktxt.getValue()==null)){
    		filterByState(etatCombo.getValue());
    	}
    	if((ClientTxt.getText().equals("")==true)&&(typeCombo.getValue()!=null)&&(etatCombo.getValue()==null)&&(datepicktxt.getValue()==null)){
    		filterByType(typeCombo.getValue());
    	}
    	if((ClientTxt.getText().equals("")==true)&&(typeCombo.getValue()==null)&&(etatCombo.getValue()==null)&&(datepicktxt.getValue()!=null)){
    		FilterByDate(datepicktxt.getValue());
    	}
    	if((ClientTxt.getText().equals("")==true)&&(typeCombo.getValue()==null)&&(etatCombo.getValue()==null)&&(datepicktxt.getValue()==null)){
    		Alert alert = new Alert(AlertType.ERROR);
    		alert.setTitle("Error Dialog");
    		alert.setHeaderText("Veuillez saisir au moins l'un des champs de filter");
    		alert.setContentText("Nom du client,Type de demande ,etat de demande , date du demande!");

    		alert.showAndWait();
    		
    	}
    }

    @FXML
    private void refrechAction(ActionEvent event) throws IOException {
    	   FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdminDemand.fxml"));
           Parent root=loader.load();
           Scene scene=new Scene(root);
           Stage  primaryStage= new Stage();
           primaryStage.setScene(scene);
           primaryStage.show();
            Stage stage = (Stage) imageHead.getScene().getWindow();
        // do what you have to do
        stage.close();
    }
    
    @FXML
    private void agentsAction(ActionEvent event) throws IOException {
    	
    	 FXMLLoader loader=new FXMLLoader(getClass().getResource("AgetnsDemandFXML.fxml"));
         Parent root=loader.load();
         Scene scene=new Scene(root);
         Stage  primaryStage= new Stage();
         primaryStage.setScene(scene);
         primaryStage.show();
    }
    
    public void FilterByName(String name){
    	List<Demand> demands = new ArrayList<Demand>();
    	data.clear();
		try {
			demands = JsonParserDemand.getDemandListByCLientName("http://localhost:18080/Arka-web/api/demandes/filterN",name );
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	
        
		
        
        Collections.reverse(demands);
        for (Demand demand : demands ) {
         	    System.out.print(demand.getIdDemand());
         	   
         		data.add(demand);		
 			 
         
         }
        listViewDemands.setCellFactory(new Callback<ListView<Demand>, ListCell<Demand>>(){
            @Override
            public ListCell<Demand> call(ListView<Demand> args0) {
                Image img= new Image("assets/file.png");
                ImageView imageview=new ImageView(img);
                Label label=new Label("");
                JFXButton buttClient=new JFXButton();
                //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
                buttClient.setStyle("-fx-background-image: url('/assets/info.png');-fx-font-size: 1em; ");
                buttClient.setMinHeight(30);
                buttClient.setMinWidth(30);
                JFXButton buttProfil=new JFXButton();
               // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
               buttProfil.setMinHeight(30);
                buttProfil.setMinWidth(32);
                JFXButton buttAffecter=new JFXButton();
                
                buttAffecter.setMinHeight(30);
                buttAffecter.setMinWidth(30);
                buttAffecter.setStyle("-fx-background-image: url('/assets/send.png');");
                buttAffecter.setAlignment(Pos.CENTER_RIGHT);
                JFXButton buttCartons=new JFXButton();
                buttCartons.setStyle("-fx-background-image: url('/assets/archive.png');");
                buttCartons.setMinHeight(30);
                buttCartons.setMinWidth(32);
                HBox hbox = new HBox();
                hbox.getChildren().addAll(imageview, label,buttProfil,buttClient,buttAffecter,buttCartons);
                
              
                
                
                
                ListCell<Demand> cell;
                int i = 0 ;
                cell = new ListCell<Demand>(){
                    
                    @Override
                    protected void updateItem(Demand r ,boolean b){
                 	  
                        super.updateItem(r,b);
                        
                          if(r != null){
                        	  buttCartons.setOnAction((event)->{
                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("cartonListFXML.fxml"));
                        		  Parent root;
                        		  try {
                        			  
									root = (Parent) loader.load();
									CartonListFXMLController clfc= loader.getController();
									clfc.setCLientName(r.getClient().getNom());
									clfc.setDate(r.getDate()+"");
									System.out.println("sizee = "+r.getCartons().size());
									clfc.fillListCartons(r.getCartons());
									  Scene newScene = new Scene(root);
	                                  Stage newStage = new Stage();
	                                  newStage.setTitle("Informations");
	                                  newStage.setScene(newScene);
	                                  newStage.show();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
									  
                        		  
                        	  });
                        	  buttClient.setOnAction((event) -> {
                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("ClientInformations.fxml"));
                        		  
                                  Parent root;
								try {
									root = (Parent) loader.load();
									ClientInformationsController ctrl = loader.getController();
	                                  ctrl.setAdresse(r.getClient().getAdress());
	                                  ctrl.setnomText(r.getClient().getNom());
	                                  ctrl.setmailtext(r.getClient().getEmail());
	                                  ctrl.setTelTxt(r.getClient().getNumTel());
	                                  ctrl.setRespText(r.getClient().getNomResp());
	                                  ctrl.setCleint(r.getClient().getIdClient());
	                                  Scene newScene = new Scene(root);
	                                  Stage newStage = new Stage();
	                                  newStage.setTitle("Informations");
	                                  newStage.setScene(newScene);
	                                  newStage.show();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
                                  
                        		  
                        	  });
                         	 if(r.getDemandState()==DemandState.nonTraitee){
                         		
                         		 hbox.setStyle("-fx-background-color:#ff9999;");
                       	   }else if(r.getDemandState()==DemandState.affectee){
                       		buttAffecter.setDisable(true);
                       		 hbox.setStyle("-fx-background-color:#fda36f;");
                       	   }else{
                       		hbox.setStyle("-fx-background-color:#bef67a;");
                       		   
                       	   }
                            label.setText("Client:" +r.getClient().getNom()+" Nom :"+r.getClient().getNomResp() + " Type: "+r.getDemandType()+ " Date :"+r.getDate());
                            setGraphic(hbox);}
                            
                          
                          
                       
                              
                            
                           
                            
                             
                            
                            
                        
                     
                        }
                        
                      
                 
                };
                
                
             return cell;    
            }
             
           
        
                 });
        listViewDemands.setItems(data);
         
       
    	
    	
    }
    
    public void FilterByDate(LocalDate date){
    	List<Demand> demands = new ArrayList<Demand>();
    	data.clear();
		try {
			demands = JsonParserDemand.getByDate("http://localhost:18080/Arka-web/api/demandes/date",date);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
        Collections.reverse(demands);
        for (Demand demand : demands ) {
         	    System.out.print(demand.getIdDemand());
         	   
         		data.add(demand);		
 			 
         
         }
        listViewDemands.setCellFactory(new Callback<ListView<Demand>, ListCell<Demand>>(){
            @Override
            public ListCell<Demand> call(ListView<Demand> args0) {
                Image img= new Image("assets/file.png");
                ImageView imageview=new ImageView(img);
                Label label=new Label("");
                JFXButton buttClient=new JFXButton();
                //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
                buttClient.setStyle("-fx-background-image: url('/assets/info.png');-fx-font-size: 1em; ");
                buttClient.setMinHeight(30);
                buttClient.setMinWidth(30);
                JFXButton buttProfil=new JFXButton();
               // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
               buttProfil.setMinHeight(30);
                buttProfil.setMinWidth(32);
                JFXButton buttAffecter=new JFXButton();
                
                buttAffecter.setMinHeight(30);
                buttAffecter.setMinWidth(30);
                buttAffecter.setStyle("-fx-background-image: url('/assets/send.png');");
                buttAffecter.setAlignment(Pos.CENTER_RIGHT);
               
                JFXButton buttCartons=new JFXButton();
                buttCartons.setStyle("-fx-background-image: url('/assets/archive.png');");
                buttCartons.setMinHeight(30);
                buttCartons.setMinWidth(32);
                HBox hbox = new HBox();
                hbox.getChildren().addAll(imageview, label,buttProfil,buttClient,buttAffecter,buttCartons);
                
                
                ListCell<Demand> cell;
                int i = 0 ;
                cell = new ListCell<Demand>(){
                    
                    @Override
                    protected void updateItem(Demand r ,boolean b){
                 	  
                        super.updateItem(r,b);
                        
                          if(r != null){
                        	  buttCartons.setOnAction((event)->{
                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("cartonListFXML.fxml"));
                        		  Parent root;
                        		  try {
                        			  
									root = (Parent) loader.load();
									CartonListFXMLController clfc= loader.getController();
									clfc.setCLientName(r.getClient().getNom());
									clfc.setDate(r.getDate()+"");
									System.out.println("sizee = "+r.getCartons().size());
									clfc.fillListCartons(r.getCartons());
									  Scene newScene = new Scene(root);
	                                  Stage newStage = new Stage();
	                                  newStage.setTitle("Informations");
	                                  newStage.setScene(newScene);
	                                  newStage.show();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
									  
                        		  
                        	  });
                        	  buttClient.setOnAction((event) -> {
                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("ClientInformations.fxml"));
                        		  
                                  Parent root;
								try {
									root = (Parent) loader.load();
									ClientInformationsController ctrl = loader.getController();
	                                  ctrl.setAdresse(r.getClient().getAdress());
	                                  ctrl.setnomText(r.getClient().getNom());
	                                  ctrl.setmailtext(r.getClient().getEmail());
	                                  ctrl.setTelTxt(r.getClient().getNumTel());
	                                  ctrl.setRespText(r.getClient().getNomResp());
	                                  ctrl.setCleint(r.getClient().getIdClient());
	                                  Scene newScene = new Scene(root);
	                                  Stage newStage = new Stage();
	                                  newStage.setTitle("Informations");
	                                  newStage.setScene(newScene);
	                                  newStage.show();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
                                  
                        		  
                        	  });
                         	 if(r.getDemandState()==DemandState.nonTraitee){
                         		
                         		 hbox.setStyle("-fx-background-color:#ff9999;");
                       	   }else if(r.getDemandState()==DemandState.affectee){
                       		buttAffecter.setDisable(true);
                       		 hbox.setStyle("-fx-background-color:#fda36f;");
                       	   }else{
                       		hbox.setStyle("-fx-background-color:#bef67a;");
                       		   
                       	   }
                            label.setText("Client:" +r.getClient().getNom()+" Nom :"+r.getClient().getNomResp() + " Type: "+r.getDemandType()+ " Date :"+r.getDate());
                            setGraphic(hbox);}
 
                        }
                        
                      
                 
                };
                
                
             return cell;    
            }
             
           
        
                 });
        listViewDemands.setItems(data);
    	
    }
    
    public void filterByType(DemandType demandType){
    	List<Demand> demands = new ArrayList<Demand>();
    	data.clear();
		try {
			demands = JsonParserDemand.getByType("http://localhost:18080/Arka-web/api/demandes/type",demandType);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fillListView(demands);
    }
    
   public void  filterByTypeState(DemandType demandType,DemandState demandState) throws ClientProtocolException, IOException{
	   List<Demand> demandsState = new ArrayList<Demand>();
	   List<Demand> demandsType = new ArrayList<Demand>();
	   List<Demand> demands = new ArrayList<Demand>();
	   demandsState = JsonParserDemand.getByState("http://localhost:18080/Arka-web/api/demandes/state",demandState);
	   demandsType = JsonParserDemand.getByType("http://localhost:18080/Arka-web/api/demandes/type",demandType);
       for (Demand demand : demandsType) {
		   for (Demand demand1 : demandsState) {
			   if(demand.equals(demand1)){
				   demands.add(demand1);
			   }
		}
	}
   	fillListView(demands);
   }
   public void  filterByClientEtat(String name,DemandState demandState) throws ClientProtocolException, IOException{
	   List<Demand> demandsClient = new ArrayList<Demand>();
	   List<Demand> demandsState = new ArrayList<Demand>();
	   List<Demand> demands = new ArrayList<Demand>();
	   demandsClient =JsonParserDemand.getDemandListByCLientName("http://localhost:18080/Arka-web/api/demandes/filterN",name );
	   demandsState = JsonParserDemand.getByState("http://localhost:18080/Arka-web/api/demandes/state",demandState);
	   for (Demand demand : demandsClient) {
		for (Demand demand1 :demandsState ) {
			if(demand.equals(demand)){
				demands.add(demand);	
			}
		}
	}
	   fillListView(demands);
   }
   public void  filterByClientTypeEtat(String clientName,DemandType demandType,DemandState demandState) throws ClientProtocolException, IOException{
	   List<Demand> demandsState = new ArrayList<Demand>();
	   List<Demand> demandsType = new ArrayList<Demand>();
	   List<Demand> demandsClient = new ArrayList<Demand>();
	   List<Demand> demands = new ArrayList<Demand>();
	   demandsState = JsonParserDemand.getByState("http://localhost:18080/Arka-web/api/demandes/state",demandState);
	   demandsType = JsonParserDemand.getByType("http://localhost:18080/Arka-web/api/demandes/type",demandType);
	   demandsClient =JsonParserDemand.getDemandListByCLientName("http://localhost:18080/Arka-web/api/demandes/filterN",clientName );
	   for (Demand demand : demandsType) {
		   for (Demand demand1 : demandsState) {
			   for (Demand demand2 : demandsClient) {
				   if((demand.equals(demand1))&&(demand.equals(demand2))){
					   demands.add(demand);
				   }

			}
			  		}
	}
   	fillListView(demands);
   }
   public void filterByClientDate(String name,LocalDate date) throws ClientProtocolException, IOException{
	   List<Demand> demandClient = new ArrayList<Demand>();
	   List<Demand> demandsDate = new ArrayList<Demand>();
	   List<Demand> demands = new ArrayList<Demand>();
	   demandClient =JsonParserDemand.getDemandListByCLientName("http://localhost:18080/Arka-web/api/demandes/filterN",name );
	   demandsDate = JsonParserDemand.getByDate("http://localhost:18080/Arka-web/api/demandes/date",date);
	   for (Demand demand : demandClient) {
		for (Demand demand1 : demandsDate) {
			if(demand.equals(demand1)){
				
			}
		}
	}
   }
   
  public void filterByClientEtatDate(String name,DemandState state,LocalDate date) throws ClientProtocolException, IOException{
	   List<Demand> demandClient = new ArrayList<Demand>();
	   List<Demand> demandsState = new ArrayList<Demand>();
	   List<Demand> demandsDate = new ArrayList<Demand>();
	   List<Demand> demands= new ArrayList<Demand>();
	   demandClient =JsonParserDemand.getDemandListByCLientName("http://localhost:18080/Arka-web/api/demandes/filterN",name );
	   demandsState = JsonParserDemand.getByState("http://localhost:18080/Arka-web/api/demandes/state",state);
	   demandsDate = JsonParserDemand.getByDate("http://localhost:18080/Arka-web/api/demandes/date",date);
	   for (Demand demand : demandClient) {
		for (Demand demand1 : demandsState) {
			for (Demand demand2 : demandsDate) {
					if((demand.equals(demand))&&(demand1.equals(demand2))){
						demands.add(demand);
					}
			}
		}
	}
	   fillListView(demands); 
   }
   public void filterByClientTypeDateEtat(String name,DemandType demandType,LocalDate date,DemandState demandState) throws ClientProtocolException, IOException{
	   List<Demand> demandClient = new ArrayList<Demand>();
	   List<Demand> demandsType = new ArrayList<Demand>();
	   List<Demand> demandsDate = new ArrayList<Demand>();
	   List<Demand> demandsEtat = new ArrayList<Demand>();
	   List<Demand> demands = new ArrayList<Demand>();
	   demandClient =JsonParserDemand.getDemandListByCLientName("http://localhost:18080/Arka-web/api/demandes/filterN",name );
	   demandsType = JsonParserDemand.getByType("http://localhost:18080/Arka-web/api/demandes/type",demandType);
	   demandsDate = JsonParserDemand.getByDate("http://localhost:18080/Arka-web/api/demandes/date",date);
	   demandsEtat = JsonParserDemand.getByState("http://localhost:18080/Arka-web/api/demandes/state",demandState);
	  
	   for (Demand demand : demandClient) {
		   for (Demand demand1 : demandsType) {
				for (Demand demand2 : demandsDate) {
					for (Demand demand3 : demandsEtat) {
						if((demand.equals(demand1))&&(demand2.equals(demand1))&&(demand2.equals(demand3))){
							demands.add(demand1);
						}
					}
					
				}
			}
	}
	   fillListView(demands); 
	   
   }
  public void filterByClientTypeDate(String name,DemandType demandType,LocalDate date) throws ClientProtocolException, IOException{
	  List<Demand> demandClient = new ArrayList<Demand>();
	   List<Demand> demandsType = new ArrayList<Demand>();
	   List<Demand> demandsDate = new ArrayList<Demand>();
	   List<Demand> demands = new ArrayList<Demand>();
	   demandClient =JsonParserDemand.getDemandListByCLientName("http://localhost:18080/Arka-web/api/demandes/filterN",name );
	   demandsType = JsonParserDemand.getByType("http://localhost:18080/Arka-web/api/demandes/type",demandType);
	   demandsDate = JsonParserDemand.getByDate("http://localhost:18080/Arka-web/api/demandes/date",date);
	  
	   for (Demand demand : demandClient) {
		   for (Demand demand1 : demandsType) {
				for (Demand demand2 : demandsDate) {
					if((demand.equals(demand1))&&(demand2.equals(demand1))){
						demands.add(demand1);
					}
				}
			}
	}
	   fillListView(demands); 
  }
  public void  filterByEtatDate(DemandState demandState,LocalDate date) throws ClientProtocolException, IOException{
	  List<Demand> demandsState = new ArrayList<Demand>();
	  
	   List<Demand> demandsDate = new ArrayList<Demand>();
	   List<Demand> demands = new ArrayList<Demand>();
	   demandsState = JsonParserDemand.getByState("http://localhost:18080/Arka-web/api/demandes/state",demandState);
	   demandsDate = JsonParserDemand.getByDate("http://localhost:18080/Arka-web/api/demandes/date",date);
       for (Demand demand : demandsState) {
		for (Demand demand1 : demandsDate) {
			if(demand1.equals(demand)){
				demands.add(demand);
			}
		}
	}
       fillListView(demands);
  }
  
  public void filterByTypeDate(DemandType demandType,LocalDate date) throws ClientProtocolException, IOException{
	  List<Demand> demandsType = new ArrayList<Demand>();
	   List<Demand> demandsDate = new ArrayList<Demand>();
	   List<Demand> demands = new ArrayList<Demand>();
	   demandsType = JsonParserDemand.getByType("http://localhost:18080/Arka-web/api/demandes/type",demandType);
	   demandsDate = JsonParserDemand.getByDate("http://localhost:18080/Arka-web/api/demandes/date",date);
  
	   for (Demand demand : demandsType) {
		   for (Demand demand1 : demandsDate) {
			   if(demand.equals(demand1)){
				   demands.add(demand1);
			   }
		}
		
	}
	   fillListView(demands); 
  }
  
  public void filterByClientType(String name,DemandType demandType) throws ClientProtocolException, IOException{
	  List<Demand> demandsType = new ArrayList<Demand>();
	  List<Demand> demandClient = new ArrayList<Demand>();
	  List<Demand> demands = new ArrayList<Demand>();
	  demandsType = JsonParserDemand.getByType("http://localhost:18080/Arka-web/api/demandes/type",demandType);
	  demandClient =JsonParserDemand.getDemandListByCLientName("http://localhost:18080/Arka-web/api/demandes/filterN",name );
      for (Demand demand : demandsType) {
		for (Demand demand1 : demandClient) {
			if(demand.equals(demand1)){
				demands.add(demand1);
			}
		}
	}
      fillListView(demands);
  }
 public void  filterByTypeEtatDate(DemandType demandType,DemandState demandState,LocalDate date) throws ClientProtocolException, IOException{
	 List<Demand> demandsState = new ArrayList<Demand>();
	   List<Demand> demandsType = new ArrayList<Demand>();
	   List<Demand> demandsDate = new ArrayList<Demand>();
	   List<Demand> demands = new ArrayList<Demand>();
	   demandsState = JsonParserDemand.getByState("http://localhost:18080/Arka-web/api/demandes/state",demandState);
	   demandsType = JsonParserDemand.getByType("http://localhost:18080/Arka-web/api/demandes/type",demandType);
	   demandsDate = JsonParserDemand.getByDate("http://localhost:18080/Arka-web/api/demandes/date",date);
	
	   for (Demand demand : demandsType) {
		   for (Demand demand1 : demandsState) {
			   for (Demand demand2 : demandsDate) {

				   if((demand.equals(demand1))&&(demand.equals(demand2))){
					   demands.add(demand1);
				   }
			}
		}
	}
	  
 	fillListView(demands);
   }
    public void filterByState(DemandState demandState){
    	List<Demand> demands = new ArrayList<Demand>();
    	data.clear();
		try {
			demands = JsonParserDemand.getByState("http://localhost:18080/Arka-web/api/demandes/state",demandState);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fillListView(demands);
    }
    
    public void fillListView(List<Demand> demands){
    	data.clear();
	        
        Collections.reverse(demands);
        for (Demand demand : demands ) {
         	    System.out.print(demand.getIdDemand());
         	   
         		data.add(demand);		
 			 
         
         }
        listViewDemands.setCellFactory(new Callback<ListView<Demand>, ListCell<Demand>>(){
            @Override
            public ListCell<Demand> call(ListView<Demand> args0) {
                Image img= new Image("assets/file.png");
                ImageView imageview=new ImageView(img);
                Label label=new Label("");
                JFXButton buttClient=new JFXButton();
                //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
                buttClient.setStyle("-fx-background-image: url('/assets/info.png');-fx-font-size: 1em; ");
                buttClient.setMinHeight(30);
                buttClient.setMinWidth(30);
                JFXButton buttProfil=new JFXButton();
               // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
               buttProfil.setMinHeight(30);
                buttProfil.setMinWidth(32);
                JFXButton buttAffecter=new JFXButton();
                
                buttAffecter.setMinHeight(30);
                buttAffecter.setMinWidth(30);
                buttAffecter.setStyle("-fx-background-image: url('/assets/send.png');");
                buttAffecter.setAlignment(Pos.CENTER_RIGHT);
                JFXButton buttCartons=new JFXButton();
                buttCartons.setStyle("-fx-background-image: url('/assets/archive.png');");
                buttCartons.setMinHeight(30);
                buttCartons.setMinWidth(32);
                HBox hbox = new HBox();
                hbox.getChildren().addAll(imageview, label,buttProfil,buttClient,buttAffecter,buttCartons);
                
                
                
                ListCell<Demand> cell;
                int i = 0 ;
                cell = new ListCell<Demand>(){
                    
                    @Override
                    protected void updateItem(Demand r ,boolean b){
                 	  
                        super.updateItem(r,b);
                        
                          if(r != null){
                        	  buttCartons.setOnAction((event)->{
                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("cartonListFXML.fxml"));
                        		  Parent root;
                        		  try {
                        			  
									root = (Parent) loader.load();
									CartonListFXMLController clfc= loader.getController();
									clfc.setCLientName(r.getClient().getNom());
									clfc.setDate(r.getDate()+"");
									System.out.println("sizee = "+r.getCartons().size());
									clfc.fillListCartons(r.getCartons());
									  Scene newScene = new Scene(root);
	                                  Stage newStage = new Stage();
	                                  newStage.setTitle("Informations");
	                                  newStage.setScene(newScene);
	                                  newStage.show();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
									  
                        		  
                        	  });
                        	  buttClient.setOnAction((event) -> {
                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("ClientInformations.fxml"));
                        		  
                                  Parent root;
								try {
									root = (Parent) loader.load();
									ClientInformationsController ctrl = loader.getController();
	                                  ctrl.setAdresse(r.getClient().getAdress());
	                                  ctrl.setnomText(r.getClient().getNom());
	                                  ctrl.setmailtext(r.getClient().getEmail());
	                                  ctrl.setTelTxt(r.getClient().getNumTel());
	                                  ctrl.setRespText(r.getClient().getNomResp());
	                                  ctrl.setCleint(r.getClient().getIdClient());
	                                  Scene newScene = new Scene(root);
	                                  Stage newStage = new Stage();
	                                  newStage.setTitle("Informations");
	                                  newStage.setScene(newScene);
	                                  newStage.show();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
                                  
                        		  
                        	  });
                         	 if(r.getDemandState()==DemandState.nonTraitee){
                         		
                         		 hbox.setStyle("-fx-background-color:#ff9999;");
                       	   }else if(r.getDemandState()==DemandState.affectee){
                       		buttAffecter.setDisable(true);
                       		 hbox.setStyle("-fx-background-color:#fda36f;");
                       	   }else{
                       		hbox.setStyle("-fx-background-color:#bef67a;");
                       		   
                       	   }
                            label.setText("Client:" +r.getClient().getNom()+" Nom :"+r.getClient().getNomResp() + " Type: "+r.getDemandType()+ " Date :"+r.getDate());
                            setGraphic(hbox);}
                            
  
                     
                        }
                        
                      
                 
                };
                
                
             return cell;    
            }
             
           
        
                 });
        listViewDemands.setItems(data);
    	
    }
}