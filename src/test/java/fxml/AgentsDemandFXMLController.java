
package fxml;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;

import arka.domain.Agent;
import arka.domain.Demand;
import arka.domain.DemandState;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.http.client.ClientProtocolException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import jsonrRsources.JsonParserAgent;
import jsonrRsources.JsonParserDemand;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class AgentsDemandFXMLController implements Initializable {

    @FXML
    private JFXComboBox<Agent> AgentsCombo;
    @FXML
    private JFXListView<Demand> DemandAgentList;
    private ObservableList<Demand> data=FXCollections.observableArrayList();
    @FXML
    private JFXButton RechercheBtn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	List<Agent> agents = new ArrayList<Agent>();
		try {
			agents = JsonParserAgent.getAgentList("http://localhost:18080/Arka-web/api/user");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		AgentsCombo.getItems().addAll(agents);
    }    

    @FXML
    private void rechAction(ActionEvent event) {
    	System.out.println(AgentsCombo.getValue().getMatricule());
    	data.clear();
   	 HBox hbox = new HBox();
   	  
   	List<Demand> demands = new ArrayList<Demand>();
		try {
			System.out.println("http://localhost:18080/Arka-web/api/demandes/DemandsClient/"+ClientInformationsController.c);
			demands = JsonParserDemand.getDemandListByAgent("http://localhost:18080/Arka-web/api/demandes/DemandsAgent",AgentsCombo.getValue().getMatricule());
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		  Collections.reverse(demands);
	        for (Demand demand : demands ) {
	         	    System.out.println(demand.getIdDemand());
	         	   
	         		data.add(demand);		
	 			 
	         
	         }
	        
	        DemandAgentList.setCellFactory(new Callback<ListView<Demand>, ListCell<Demand>>(){
	            @Override
	            public ListCell<Demand> call(ListView<Demand> args0) {
	                Image img= new Image("assets/file.png");
	                ImageView imageview=new ImageView(img);
	                Label label=new Label("");
	                JFXButton buttClient=new JFXButton();
	                //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
	               
	                JFXButton buttProfil=new JFXButton();
	               // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
	               buttProfil.setMinHeight(30);
	                buttProfil.setMinWidth(32);
	                JFXButton buttAffecter=new JFXButton();
	                
	             
	                buttAffecter.setAlignment(Pos.CENTER_RIGHT);
	                HBox hbox = new HBox();
	                hbox.getChildren().addAll(imageview, label,buttProfil,buttClient,buttAffecter);
	                
	                
	                
	                ListCell<Demand> cell;
	                int i = 0 ;
	                cell = new ListCell<Demand>(){
	                    
	                    @Override
	                    protected void updateItem(Demand r ,boolean b){
	                 	  
	                        super.updateItem(r,b);
	                        
	                          if(r != null){
	                        	  buttClient.setOnAction((event) -> {
	                        		  FXMLLoader loader = new FXMLLoader(getClass().getResource("ClientInformations.fxml"));
	                        		  
	                                  Parent root;
									try {
										root = (Parent) loader.load();
										ClientInformationsController ctrl = loader.getController();
		                                  ctrl.setAdresse(r.getClient().getAdress());
		                                  ctrl.setnomText(r.getClient().getNom());
		                                  ctrl.setmailtext(r.getClient().getEmail());
		                                  ctrl.setTelTxt(r.getClient().getNumTel());
		                                  ctrl.setRespText(r.getClient().getNomResp());
		                                  ctrl.setCleint(r.getClient().getIdClient());
		                                  Scene newScene = new Scene(root);
		                                  Stage newStage = new Stage();
		                                  newStage.setTitle("Informations");
		                                  newStage.setScene(newScene);
		                                  newStage.show();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
	                                  
	                        		  
	                        	  });
	                         	 if(r.getDemandState()==DemandState.nonTraitee){
	                         		
	                         		 hbox.setStyle("-fx-background-color:#ff9999;");
	                       	   }else if(r.getDemandState()==DemandState.affectee){
	                       		buttAffecter.setDisable(true);
	                       		 hbox.setStyle("-fx-background-color:#fda36f;");
	                       	   }else{
	                       		hbox.setStyle("-fx-background-color:#bef67a;");
	                       		   
	                       	   }
	                            label.setText("Client:" +r.getClient().getNom()+" Nom :"+r.getClient().getNomResp() + " Type: "+r.getDemandType()+ " Date :" + r.getDate()+ " Date d'affectation : " +r.getAssignmentDate());
	                            setGraphic(hbox);}
	                            
	                          
	                          
	                       
	                              
	                            
	                           
	                            
	                             
	                            
	                            
	                        
	                     
	                        }
	                        
	                      
	                 
	                };
	                
	                
	             return cell;    
	            }
	             
	           
	        
	                 });
	        DemandAgentList.setItems(data);
    }
    
}
