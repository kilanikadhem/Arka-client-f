/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.ejb.EJB;

import org.apache.http.client.ClientProtocolException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;

import arka.domain.Admin;
import arka.domain.Agent;
import arka.domain.Carton;
import arka.domain.Demand;
import arka.service.AgentService;
import arka.service.CartonService;
import arka.service.EmplacementService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import jsonrRsources.JsonParserAgent;
import jsonrRsources.JsonParserDemand;
/**
 * FXML Controller class
 *
 * @author Ahmed
 */
public class MenuAffectDemandController implements Initializable {

    @FXML private Label demandType ; 
    @FXML private Label emplacement; 
    @FXML private Label nbcartons ; 
    @FXML private JFXButton btnAffect ;
    @FXML private JFXComboBox<Agent> agentList ; 
    @FXML private Label archivist ; 

    @EJB 
    CartonService cs ;
    @EJB
    EmplacementService es;
    @EJB
    AgentService as ;
    
    
    

    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	List<Agent> agents = new ArrayList<Agent>();
		try {
			agents = JsonParserAgent.getAgentList("http://localhost:18080/Arka-web/api/user");
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		agentList.getItems().addAll(agents);
    }   

    
    @FXML
    public void sendIdDemand(int IdDemand){
    	
    }
    

    
      @FXML
    private void affecterAction(ActionEvent event) throws IOException {
       FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAffectDemand.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();

    }

	public void setAdmin(String nomArch) {
		archivist.setText(nomArch);
	}

	public void setAgent(Agent agent) {
		
		
	}

	public void setCarton(int idDemand) {
		Demand demande = new Demand();
		Carton carton = new Carton(); 
		try {
			demande = JsonParserDemand.getDemand("http://localhost:18080/Arka-web/api/demandes"+ idDemand);
			List<Carton>cartons = demande.getCartons();
		}
		catch (ClientProtocolException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		emplacement.setText(carton.getLocale().getLine()+ " , " +carton.getLocale().getRow());
		
	}
}
