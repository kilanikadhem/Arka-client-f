/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;

import arka.domain.Agent;
import arka.domain.Client;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.http.client.ClientProtocolException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import jsonrRsources.JsonParserAgent;
import jsonrRsources.JsonParserClient;

/**
 * FXML Controller class
 *
 * @author Asusu
 */
public class GetlistclientController implements Initializable {
    @FXML
    private ImageView imageHead;
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private JFXButton listclientbtn;
    @FXML
    private ImageView HomeImg;
    @FXML
    private JFXButton ajouterclientbtn ;
    @FXML
    private JFXButton profil;
    @FXML
    private ImageView personnelImg;
    @FXML
    private ImageView personnelImg1;
    @FXML
    private JFXListView<Client> listclient;
    @FXML
    private ImageView LogoutImg;

    /**
     * Initializes the controller class.
     */
    private ObservableList<Client> data=FXCollections.observableArrayList();
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	data.clear();
	    	 HBox hbox = new HBox();
	    	List<Client> clients= new ArrayList<Client>();
			try {
				clients  = JsonParserClient.getClientList("http://localhost:18080/Arka-web/api/client");
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	
	
			Collections.reverse(clients );
	        for (Client client: clients ) {
	                System.out.print(client.getNom());
	                data.add(client);       
	             
	         
	         }
	        listclient.setCellFactory(new Callback<ListView<Client>, ListCell<Client>>(){
	            @Override
	            public ListCell<Client> call(ListView<Client> args0) {
	            	Image img= new Image("assets/file.png");
	                ImageView imageview=new ImageView(img);
	         
	                Label label=new Label("");
	               JFXButton buttdelete=new JFXButton();
	                buttdelete.setStyle("-fx-background-image: url('/assets/delete.png');");
	                buttdelete.setMinHeight(30);
	                buttdelete.setMinWidth(30);
	                
	               
	                //JFXButton emptybtn=new JFXButton();
	                //emptybtn.setMinHeight(20);
	               // emptybtn.setMinWidth(20);
	               // emptybtn.setStyle("fx-background-image: url('/assets/delete.png');");
	                //emptybtn.setDisable(true);
	                //emptybtn.setAlignment(Pos.CENTER_RIGHT);
	               
	                //JFXButton buttProfil=new JFXButton();
	               // buttProfil.setStyle("-fx-background-image: url('/assets/send.png');");
	               // buttProfil.setMinHeight(20);
	               // buttProfil.setMinWidth(20);
	               
	                
	               
	               
	                HBox hbox = new HBox();
	                hbox.getChildren().addAll(imageview, label,buttdelete/*,buttProfil*/);
	                ListCell<Client> cell;
	                int i = 0 ;
	                cell = new ListCell<Client>(){
	                   
	                	@Override
	                    protected void updateItem(Client r ,boolean b){
	                      
	                        super.updateItem(r,b);
	                        
	                          if(r != null){
	                        	  buttdelete.setOnAction((event) -> {
	                        		 
	                        				
	                        			try {
											
											Alert alert = new Alert(AlertType.CONFIRMATION);
											alert.setTitle("Confirmation Dialog");
											alert.setHeaderText("Look, a Confirmation Dialog");
											alert.setContentText("Are you ok with this?");

											Optional<ButtonType> result = alert.showAndWait();
											if (result.get() == ButtonType.OK){
												JsonParserClient.DeleteClient(r.getIdClient());
												Stage stage = (Stage) imageHead.getScene().getWindow();
											     stage.close();
											     FXMLLoader loader=new FXMLLoader(getClass().getResource("getlistclient.fxml"));
									             Parent root=loader.load();
									        Scene scene=new Scene(root);
									        Stage  primaryStage= new Stage();
									        primaryStage.setScene(scene);
									        primaryStage.show();
											}
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
	                        	    	
	                        		  
	                        		
	                        	  });
	                            /*s  buttProfil.setOnAction((event) -> {
	                                  FXMLLoader loader = new FXMLLoader(getClass().getResource("updateAgent.fxml"));
	                                  
	                                  Parent root;
	                                  
	                                try {
                                      root = (Parent) loader.load();
                                      UpdateAgentController update= loader.getController();          
	                                	  update.setnomtxt(r.getNom());
	                                	  update.setemailtxt(r.getEmail());
	                                	  update.setprenomtxt(r.getPrenom());
	                                      update.setmatricule(r.getMatricule());
	                                      update.setnumtel(r.getNumTel());
  	                                  Scene newScene = new Scene(root);
	                                      Stage newStage = new Stage();
	                                      newStage.setTitle("Informations");
	                                      newStage.setScene(newScene);
	                                      newStage.show();
	                                } catch (Exception e) {
	                                    // TODO Auto-generated catch block
	                                    e.printStackTrace();
	                                }
	                                  
	                                  
	                              });*/ 
	                            
	                            label.setText(" Client: " +r.getIdClient()+"   code : "+r.getCodeClient() + "   nom "+r.getNom()+ "   email: "+r.getEmail()+"   nom responsable: "+r.getNomResp());
	                            setGraphic(hbox);}

	                        }
	                      };
	             return cell;    
	            }
                     });
	        listclient.setItems(data);
	        
	    }
        

    @FXML
    private void homeAction(ActionEvent event)throws IOException {
    	  FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
          Parent root=loader.load();
          Scene scene=new Scene(root);
          Stage  primaryStage= new Stage();
          primaryStage.setScene(scene);
          primaryStage.show();
          Stage stage = (Stage) imageHead.getScene().getWindow();
          stage.close();
    }

    @FXML
    private void logoutAction(ActionEvent event) throws IOException{
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Login.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void getlistclient(ActionEvent event) {
    }

    @FXML
    private void ajouterclient(ActionEvent event) throws IOException{
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ajouterclient.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void showProfil(ActionEvent event)throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Profile.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }
    
}
