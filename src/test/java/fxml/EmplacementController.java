/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import javafx.scene.text.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jsonrRsources.JsonParserCarton;
import jsonrRsources.JsonParserEmplacement;
import javafx.scene.input.KeyEvent;
import javafx.event.EventHandler;
/**
 * FXML Controller class
 *
 * @author hafedh
 */
public class EmplacementController implements Initializable {

    /**
     * Initializes the controller class.
     */
	private int idsite;
	@FXML
    private Text testallee;
	@FXML
    private Text testetage;
	@FXML
    private Text testcolonne;
	@FXML
    private JFXTextField id_allee;
	@FXML
    private JFXTextField id_etage;
	@FXML
    private JFXTextField id_colonne;
	
	@FXML
    private JFXButton idajouteremplacement;
	@FXML
    private ImageView imageHead;
	@FXML
    private ImageView cartonsImg;
	
	@FXML
    private ImageView id_imagecarton;
	
	@FXML
    private ImageView HomeImg;
	@FXML
    private ImageView demandesImg;
	@FXML
    private ImageView sitesImg;
	@FXML
    private ImageView personnelImg;
	@FXML
    private ImageView c;
	@FXML
    private ImageView statImg;
	@FXML
    private ImageView id_plus;
	@FXML
    private ImageView id_imgdate;
	@FXML
    private ImageView id_pluscarton;
	@FXML
    private ImageView id_location;
	@FXML
    private void ajouterEmplacement(ActionEvent event) throws IOException, JAXBException {
		int line=Integer.parseInt(id_allee.getText());
    	int row=Integer.parseInt(id_etage.getText());
    	int driveway=Integer.parseInt(id_colonne.getText());
    	boolean empty=true;
    	int a=getIdsite();
    	/*id_allee.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
    	    @Override
    	    public void handle(KeyEvent event) {
    	        
                   if(line ==(int)line)
                   	   testallee.setText("Veuillez saisir un entier");
    	            event.consume();                    
    	    }});
    	id_etage.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
    	    @Override
    	    public void handle(KeyEvent event) {
    	        
                   if(row ==(int)row)
                   	   testetage.setText("Veuillez saisir un entier");
    	            event.consume();                    
    	    }});
    	id_colonne.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
    	    @Override
    	    public void handle(KeyEvent event) {
    	        
                   if(driveway ==(int)driveway)
                   	   testcolonne.setText("Veuillez saisir un entier");
    	            event.consume();                    
    	    }});*/
		if(line !=(int)line||row !=(int)row||driveway !=(int)driveway)
		{
			Alert alert = new Alert(Alert.AlertType.ERROR);
	         alert.setHeaderText("CAPTB");
	         alert.setTitle("Confirmation");
	      
	         alert.initModality(Modality.APPLICATION_MODAL);
	         alert.setContentText("Veuillez saisir un entier");
	         		
	         Button exitButton = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
	         exitButton.setText("Oui");
	         Optional<ButtonType> resu = alert.showAndWait();
		}
		
		JsonParserEmplacement.add_emplacement(a,line, row, driveway, empty);
		
		System.out.println("------------------------------------------");
    }
	@FXML
    private void homeAction(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void cartonsBtn(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListeCarton.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void demandesAction(ActionEvent event) {
    }

    @FXML
    private void sitesActions(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListSite.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }
    @FXML
    private void personnelAction(ActionEvent event) {
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	Image img = new Image("assets/hrhead.jpg");
		imageHead.setImage(img);
        img = new Image("assets/home.png");
        HomeImg.setImage(img);
        
        img = new Image("assets/send.png");
        demandesImg.setImage(img);
        img = new Image("assets/placeholder.png");
        sitesImg.setImage(img);
        img = new Image("assets/users.png");
        personnelImg.setImage(img);
        img = new Image("assets/projectmenu.png");
        statImg.setImage(img);
    	
		img = new Image("assets/archive.png");
		cartonsImg.setImage(img);
		img = new Image("assets/document-add-icon.png");
		id_plus.setImage(img);
		
		img = new Image("assets/20045.png");
		id_pluscarton.setImage(img);
		img = new Image("assets/archivage.png");
		id_location.setImage(img);
		
    }
	public int getIdsite() {
		return idsite;
	}
	public void setIdsite(int idsite) {
		this.idsite = idsite;
	}    
    
}
