/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fxml;

import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import arka.domain.Site;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import jsonrRsources.JsonParserCarton;
import jsonrRsources.JsonParserSite;

/**
 * FXML Controller class
 *
 * @author hafedh
 */
public class SiteMngmtController implements Initializable {

    /**
     * Initializes the controller class.
     */
	@FXML
    private ImageView imageHead;
	@FXML
    private Text id_imagecaptb;
	@FXML
    private ImageView id_imagecarton;
	@FXML
    private ImageView cartonsImg;
	
	@FXML
    private ImageView id_imagerecherche;
	@FXML
    private ImageView HomeImg;
	@FXML
    private ImageView demandesImg;
	@FXML
    private ImageView sitesImg;
	@FXML
    private ImageView personnelImg;
	
	@FXML
    private ImageView statImg;
	@FXML
    private ImageView id_pluscarton;
	@FXML
    private JFXTextField id_nom;
	@FXML
    private JFXTextField id_addresse;
	@FXML
    private JFXButton idajouter;
	@FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton CartonBtn;
    @FXML
    private JFXButton DemandesBtn;
    @FXML
    private JFXButton SitesBtn;
    
    @FXML
    private JFXButton PersonnelBtn;
	@FXML
    private void homeAction(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdmin.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void cartonsBtn(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListeCarton.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void demandesAction(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdminDemand.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void sitesActions(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListSite.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }
    @FXML
    private void personnelAction(ActionEvent event) {
    }
    @FXML
    private void ajouterSite(ActionEvent event) throws IOException, JAXBException {
    	
    	String nom=id_nom.getText();
    	String addresse=id_addresse.getText();
    	
		JsonParserSite.add_site(nom,addresse);
		System.out.println("------------------------------------------");
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	Image img = new Image("assets/hrhead.jpg");
		imageHead.setImage(img);
        img = new Image("assets/home.png");
        HomeImg.setImage(img);
        img = new Image("assets/archive.png");
        cartonsImg.setImage(img);
        img = new Image("assets/send.png");
        demandesImg.setImage(img);
        img = new Image("assets/placeholder.png");
        sitesImg.setImage(img);
        img = new Image("assets/users.png");
        personnelImg.setImage(img);
        img = new Image("assets/projectmenu.png");
        statImg.setImage(img);
    	img = new Image("assets/placeholder.png");
		id_imagecarton.setImage(img);
		img = new Image("assets/20045.png");
		id_pluscarton.setImage(img);
    }    
    
}
