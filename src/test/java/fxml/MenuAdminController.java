/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fxml;


import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import com.jfoenix.controls.JFXButton;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class MenuAdminController implements Initializable {

    @FXML
    private ImageView imageHead;
    @FXML
    private ImageView LogoutImg;
    @FXML
    private JFXButton HomeBtn;
    @FXML
    private JFXButton CartonBtn;
    @FXML
    private JFXButton DemandesBtn;
    @FXML
    private JFXButton SitesBtn;
    @FXML
    private JFXButton LogoutBtn;
    @FXML
    private JFXButton Btnmail;
    @FXML
    private JFXButton PersonnelBtn;
    @FXML
    private JFXButton clientBtn1;
    @FXML
    private ImageView HomeImg;
    @FXML
    private ImageView cartonsImg;
    @FXML
    private ImageView demandesImg;
    @FXML
    private ImageView sitesImg;
    @FXML
    private ImageView personnelImg;
    @FXML
    private ImageView statImg;
    @FXML 
    private ImageView Imgmail;
    @FXML
    private Text liveChrono;
    @FXML
    private ImageView imgNbreCarton;
    @FXML
    private ImageView imgNbDemand;
    @FXML
    private ImageView imgDemandTrait;
    @FXML
    private Text nbreCarton;
    @FXML
    private Text nbreDeamnd;
    @FXML
    private Text nbreDemandTrait;
   
    @FXML
    private MenuButton personnel;
    @FXML
    private MenuItem agentbtnitem;
    @FXML
    private MenuItem adminbtnitem;
    @FXML
    private JFXButton profil;
    /**
     * Initializes the controller class.
     */
  
    public void initialize(URL url, ResourceBundle rb) {
    	 Image img = new Image("assets/hrhead.jpg");
    	 imageHead.setImage(img);
         img = new Image("assets/logout.png");
         LogoutImg.setImage(img);
         img = new Image("assets/home.png");
         HomeImg.setImage(img);
         img = new Image("assets/archive.png");
         cartonsImg.setImage(img);
         img = new Image("assets/send.png");
         demandesImg.setImage(img);
         img = new Image("assets/placeholder.png");
         sitesImg.setImage(img);
         img = new Image("assets/users.png");
         personnelImg.setImage(img);
         img = new Image("assets/projectmenu.png");
         statImg.setImage(img);
     	
         
         SimpleDateFormat formater = null;
         Date aujourdhui = new Date();
         formater = new SimpleDateFormat("dd-MM-yy");
         liveChrono.setText(formater.format(aujourdhui));
         img = new Image("assets/archive.png");
         imgNbreCarton.setImage(img);
         img = new Image("assets/reminder.png");
         imgNbDemand.setImage(img);
         img = new Image("assets/checklist.png");
         imgDemandTrait.setImage(img);
         img = new Image("assets/at.png");
         Imgmail.setImage(img);
    }    

    @FXML
    private void homeAction(ActionEvent event) {
    }

    @FXML
    private void cartonsBtn(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListeCarton.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void demandesAction(ActionEvent event) throws IOException {
       FXMLLoader loader=new FXMLLoader(getClass().getResource("MenuAdminDemand.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
       Stage stage = (Stage) imageHead.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void sitesActions(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("ListSite.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void logoutAction(ActionEvent event) throws IOException {
        FXMLLoader loader=new FXMLLoader(getClass().getResource("Login.fxml"));
       Parent root=loader.load();
       Scene scene=new Scene(root);
       Stage  primaryStage= new Stage();
       primaryStage.setScene(scene);
       primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
    // do what you have to do
    stage.close();
    }

    @FXML
    private void personnelAction(ActionEvent event) throws IOException {
    	 FXMLLoader loader=new FXMLLoader(getClass().getResource("Agent.fxml"));
         Parent root=loader.load();
         Scene scene=new Scene(root);
         Stage  primaryStage= new Stage();
         primaryStage.setScene(scene);
         primaryStage.show();
         Stage stage = (Stage) imageHead.getScene().getWindow();
         stage.close();
    }
    
    @FXML
    private void agentservice(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Agent.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void adminservice(ActionEvent event) throws IOException {
    	FXMLLoader loader=new FXMLLoader(getClass().getResource("Admin.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
    }
   
    @FXML
    private void MdpAction(ActionEvent event) {
    	System.out.println("sysout");
    }
   
    private void  gestionclient(ActionEvent event)throws IOException {
   	 FXMLLoader loader=new FXMLLoader(getClass().getResource("Client.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
         Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
   }
  @FXML
   private void showProfil(ActionEvent event) throws IOException {
   	 FXMLLoader loader=new FXMLLoader(getClass().getResource("Profile.fxml"));
        Parent root=loader.load();
        Scene scene=new Scene(root);
        Stage  primaryStage= new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        Stage stage = (Stage) imageHead.getScene().getWindow();
        stage.close();
   }
    
}
